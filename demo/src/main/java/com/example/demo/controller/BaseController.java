package com.example.demo.controller;


import com.example.demo.validation.LoginValidation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.Response;
import java.util.HashMap;
import java.util.Map;

@CrossOrigin(origins = "*", allowedHeaders = "*")
public class BaseController<T> {

    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, Object> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        Map<String, String[]> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String[] errorMessage = {error.getDefaultMessage()};
            errors.put(fieldName, errorMessage);
        });
        Map<String,Object> response = new HashMap<>();
        response.put("succes", false);
        response.put("message","the given data was invalid");
        response.put("errors",errors);
        return response;
    }

    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity formatResponseOk (
            T data) {
        Map<String,Object> response = new HashMap<>();
        response.put("status",true);
        response.put("message","succefuly");
        response.put("data",data);
        return new ResponseEntity(response,HttpStatus.OK);
    }

    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    public ResponseEntity formatResponseError (
            Object errors, String message) {
        Map<String,Object> response = new HashMap<>();
        response.put("succes", false);
        response.put("message",message);
        response.put("errors",errors);
        return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(response);
        //throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, String.format("No resource found for id (%s)", response));
        //return response;
    }
}
