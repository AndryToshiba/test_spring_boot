package com.example.demo.controller;

import com.example.demo.Constants;
import com.example.demo.entites.User;
import com.example.demo.services.UserService;
import com.example.demo.validation.ConfirmPasswordValidation;
import com.example.demo.validation.LoginValidation;
import com.example.demo.validation.PasswordValidation;
import com.example.demo.validation.UserValidation;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/users/")
public class UserController extends BaseController {

    @Autowired
    private UserService service;

    @PostMapping("register")
    public ResponseEntity registerUser(@Valid @RequestBody UserValidation request){
        User user = request.getInput();
        user.setPassword(encrrypt(user.getPassword()));
       return formatResponseOk(service.save(user));
    }

    @PostMapping("login")
    public ResponseEntity login(@Valid @RequestBody LoginValidation request){
        User user = service.findByEmail(request.getEmail());
        if(user == null){
            return formatResponseError(loginInvalid("email","Email was not found"),"the given data was invalid");
        }
        if(!BCrypt.checkpw(request.getPassword(), user.getPassword())){
            return formatResponseError(loginInvalid("password","Password invalid"),"the given data was invalid");
        }
        user.setApi_token(generateJWTToken(user));
        service.save(user);
        return formatResponseOk(user);
    }

    @GetMapping("login")
    public ResponseEntity noAuth(){
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Unauthorize");
    }

    @PostMapping("password")
    public ResponseEntity password(@Valid @RequestBody PasswordValidation request){
        User user = service.findByEmail(request.getEmail());
        if(user == null){
            return formatResponseError(loginInvalid("email","Email was not found"),"the given data was invalid");
        }
        Map<String, String> email = new HashMap<>();
        email.put("email",user.getEmail());
        return formatResponseOk(email);
    }

    @PostMapping("confirmPassword")
    public ResponseEntity confirmPassword(@Valid @RequestBody ConfirmPasswordValidation request){
        User user = service.findByEmail(request.getEmail());
        if(user == null){
            return formatResponseError(loginInvalid("email","Email was not found"),"the given data was invalid");
        }
        user.setPassword(encrrypt(request.getPassword()));
        user.setApi_token(generateJWTToken(user));
        service.save(user);
        return formatResponseOk(user);
    }

    private String encrrypt(String password){
        return BCrypt.hashpw(password,BCrypt.gensalt(10));

    }

    private String generateJWTToken(User user){
        long timestamp = System.currentTimeMillis();
        return Jwts.builder().signWith(SignatureAlgorithm.HS256, Constants.API_SECRET_KEY)
                .setIssuedAt(new Date(timestamp))
                .setExpiration(new Date(timestamp + Constants.TOKEN_VALIDITY))
                .claim("userId",user.getId())
                .claim("email",user.getEmail())
                .claim("lastName",user.getLastName())
                .claim("firstName",user.getFirstName())
                .compact();
    }

    private Map<String,Object> loginInvalid(String field, String error) {
        Map<String, Object> errors = new HashMap<>();
        String[] errorMessage = {error};
        errors.put(field,errorMessage);
        return errors;
    }

}
