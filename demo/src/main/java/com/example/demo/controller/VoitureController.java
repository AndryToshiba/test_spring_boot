package com.example.demo.controller;

import com.example.demo.entites.Voiture;
import com.example.demo.services.CommentaireService;
import com.example.demo.services.VoitureService;
import com.example.demo.validation.VoitureValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/voiture/")
public class VoitureController extends BaseController {

    @Autowired
    private VoitureService service;
    @Autowired
    private CommentaireService serviceCommentaire;

    @PostMapping("publier")
    public ResponseEntity addVoiture(@Valid @RequestBody VoitureValidation request){
        return formatResponseOk(service.save(request.getInput()));
    }

    @PostMapping("liste-guest")
    public ResponseEntity getVoiture(){
        List<Object> commentaire = new ArrayList<>();
        List<Object> voitures = new ArrayList<>();
        Map<String,Object> data = new HashMap<>();
        service.getAll().forEach(v->{
            data.put("id",v.getId());
            data.put("model",v.getModel());
            data.put("image",v.getImage());
            data.put("commentaire",commentaire);
            voitures.add(data);
        });
        return formatResponseOk(voitures);
    }

    @PostMapping("liste-auth")
    public ResponseEntity getVoitureCommentaire(){
        List<Object> voitures = new ArrayList<>();
        Map<String,Object> data = new HashMap<>();
        service.getAll().forEach(v->{
            data.put("id",v.getId());
            data.put("model",v.getModel());
            data.put("image",v.getImage());
            data.put("commentaire",serviceCommentaire.getByVoiture(v.getId()));
            voitures.add(data);
        });
        return formatResponseOk(voitures);
    }

    @GetMapping("getId/{id}")
    public Voiture getId(@PathVariable int id){
        return service.findById(id).orElse(null);
    }


}
