package com.example.demo.controller;

import com.example.demo.entites.Commentaire;
import com.example.demo.services.CommentaireService;
import com.example.demo.services.VoitureService;
import com.example.demo.validation.CommentaireValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Map;

@RestController
@RequestMapping("/api/commentaire/")
public class CommentaireController extends BaseController {

    @Autowired
    private CommentaireService service;
    @Autowired
    private VoitureService voitureService;

    @PostMapping("add")
    public ResponseEntity addCommentaire(HttpServletRequest httpRequest, @Valid @RequestBody CommentaireValidation request){

       return formatResponseOk(service.save(request.getInput()));

    }

    @GetMapping("all")
    public ResponseEntity getCommentaire(HttpServletRequest httpRequest){
        return formatResponseOk(service.getAll());
    }


}
