package com.example.demo.entites;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Entity
@Table(name = "commentaire")
public class Commentaire implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false)
    private String message;
    @Column(nullable = false)
    private int voitureId;

    public Commentaire(){}

    public void setId(int id) {
        this.id = id;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setVoitureId(int voitureId) {
        this.voitureId = voitureId;
    }

    public int getId() {
        return id;
    }

    public String getMessage() {
        return message;
    }

    public int getVoitureId() {
        return voitureId;
    }

    public Commentaire(String message, int voitureId) {
        this.message = message;
        this.voitureId = voitureId;
    }
}
