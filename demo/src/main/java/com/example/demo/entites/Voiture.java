package com.example.demo.entites;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "voitures")
public class Voiture implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(unique = true,nullable = false)
    private String model;
    @Column(nullable = false,columnDefinition="TEXT")
    private String image;

    public int getId() {
        return id;
    }

    public String getModel() {
        return model;
    }

    public String getImage() {
        return image;
    }

    public Voiture(){}

    public Voiture(String model,String image) {
        this.model = model;
        this.image = image;
    }
}
