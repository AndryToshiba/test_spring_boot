package com.example.demo.validation;

import com.example.demo.entites.Commentaire;
import com.example.demo.entites.Voiture;
import com.example.demo.services.VoitureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Optional;

public class CommentaireValidation {
    @Autowired
    private VoitureService serviceVoiture;

    @NotBlank(message = "Required")
    private String message;
    @NotNull(message = "Required")
    private int voitureId;

    public void setMessage(String message) {
        this.message = message;
    }

    public void setVoitureId(int voitureId) {
        this.voitureId = voitureId;
    }

    public Commentaire getInput(){
        return new Commentaire(this.message,this.voitureId);
    }
}
