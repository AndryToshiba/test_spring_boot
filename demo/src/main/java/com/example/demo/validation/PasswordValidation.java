package com.example.demo.validation;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

public class PasswordValidation {
    @Email(message = "Email invalid format")
    @NotBlank(message = "Required")
    private String email;
    public void setEmail(String email) {
        this.email = email;
    }
    public String getEmail() {
        return email;
    }
}
