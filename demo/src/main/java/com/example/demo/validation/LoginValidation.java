package com.example.demo.validation;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

public class LoginValidation {
    @Email(message = "Email invalid format")
    @NotBlank(message = "Required")
    private String email;
    @NotBlank(message = "Required")
    private String password;

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }
}
