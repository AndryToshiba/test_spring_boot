package com.example.demo.validation;

import com.example.demo.entites.Voiture;

import javax.validation.constraints.NotBlank;

public class VoitureValidation {
    @NotBlank(message = "Required")
    private String model;
    @NotBlank(message = "Required")
    private String image;

    public void setModel(String model) {
        this.model = model;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Voiture getInput(){
        return new Voiture(model,image);
    }
}
