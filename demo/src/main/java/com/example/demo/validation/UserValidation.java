package com.example.demo.validation;

import com.example.demo.entites.User;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

public class UserValidation {
    @NotBlank(message = "Required")
    private String firstName;
    @NotBlank(message = "Required")
    private String lastName;
    @Email(message = "Email invalid format")
    @NotBlank(message = "Required")
    private String email;
    @NotBlank(message = "Required")
    private String password;

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public User getInput(){
        return new User(firstName,lastName,email,password);
    }
}
