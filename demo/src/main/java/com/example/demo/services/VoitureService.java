package com.example.demo.services;

import com.example.demo.entites.Voiture;
import com.example.demo.repository.VoitureRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class VoitureService {

    @Autowired
    private VoitureRepository repository;

    public Voiture save(Voiture input){
        return repository.save(input);
    }

    public List<Voiture> getAll(){
        return repository.findAll();
    }

    public Optional<Voiture> findById(int id){
        return repository.findById(id);
    }
}
