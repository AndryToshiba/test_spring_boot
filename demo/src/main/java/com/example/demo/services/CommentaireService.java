package com.example.demo.services;

import com.example.demo.entites.Commentaire;
import com.example.demo.repository.CommentaireRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentaireService {

    @Autowired
    private CommentaireRepository repository;

    public Commentaire save(Commentaire input)
    {
        return repository.save(input);
    }

    public List<Commentaire> getAll(){
        return repository.findAll();
    }

    public List<Commentaire> getByVoiture(int id){
        return repository.getAllByVoitureId(id);
    }
}
