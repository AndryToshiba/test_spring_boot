package com.example.demo.services;

import com.example.demo.entites.User;
import com.example.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private UserRepository repository;

    public User save(User input){
        return repository.save(input);
    }

    public User findByEmail(String email){
        return repository.findByEmail(email);
    }
}
