package com.example.demo.repository;

import com.example.demo.entites.Commentaire;
import com.example.demo.entites.Voiture;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CommentaireRepository extends JpaRepository<Commentaire,Integer> {
    public List<Commentaire> getAllByVoitureId(int id);
}
