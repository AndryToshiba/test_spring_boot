/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//console.log(event.clientY,event.pageY,Math.round($(window).scrollTop()))

var b = $('body').width();
var h = $('body').height();
var el = $('body').children().length;
var w_nav = window.innerWidth;
var h_nav = window.innerHeight;
var scr_h = screen.height;
var scr_w = screen.width;
var path_name = window.location.pathname;
var base_url = 'http://' + window.location.host + '/';
var DisplayInfo, TableListe, is_action = { modifier: 1, supprimer: 2, detail: 3 }
    , mois_de_annee = ['undefined', 'Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Séptembre', 'Octobre', 'Novembre', 'Décembre'];
var callback_ajax = [], bibliotheque = {}//funct _ 





$(document).ajaxComplete(function (event, xhr, options) {
    var k = 0, dataSet = {};
    options['data'] = options.data || '';
    for (var it in options.data.split('&')) {
        dataSet[options.data.split('&')[it].split('=')[0]] = options.data.split('&')[it].split('=')[1];
    }
    /*remove title*/
    $("[sess-actif^='info-title-']").each(function () {
        $(this).remove()
    })
    /*remove title*/
    while (callback_ajax[k]) {
        if (typeof callback_ajax[k] == 'function')
            callback_ajax[k].call()
        else if (typeof callback_ajax[k] == 'object') {
            for (var h in callback_ajax[k])
                if (typeof callback_ajax[k][h] === 'function')
                    callback_ajax[k][h].call()
        }//fnct

    }
    k = 0
    if (options && xhr.status === 200 && options.callback) {
        var listner = options.listner ? options.listner : 'execute'
        while (typeof options.callback[listner] === 'object' && options.callback[listner][k]) {
            options.callback[listner][k].apply(this, [event, xhr.responseJSON, dataSet])
            k++
        }
    }
});
function addAjaxCallback(fnct) {
    if (typeof fnct === 'function')
        callback_ajax.push(fnct)
}

var generateChars = function (length) {
    var chars = '';
    for (var i = 0; i < length; i++) {
        var randomChar = Math.floor(Math.random() * 36);
        chars += randomChar.toString(36);
    }

    return chars;
};
var trimPoint = function (x) {
    x.trim()
    return x.replace(/^[.]+|[.]+$/gm, '');
} 

$(document).ready(function ($) {

    var url = window.location;
    var element = $('ul.nav a').filter(function () {
        return this.href == url || url.href.indexOf(this.href) == 0;
    }).addClass('active').parent().parent().addClass('in').parent();
    if (element.is('li')) {
        element.addClass('active');
    }

    $.fn.InlineBlock = function () {
        $(this).css('display', 'inline-block')
        return this
    }

    $.fn.DisplayNone = function () {
        $(this).css('display', 'none')
        return this
    }

    $.fn.dataFnct = function (_fnct, motif = 'execute') {
        var _this = $(this), fnct = {}, isFnct = [], push = 0
        if (typeof _fnct !== 'function')
            return _this
        for (var item in $(this).data().fnct) {
            if (motif === item) {
                while ($(this).data().fnct[item][push] && typeof $(this).data().fnct[item][push] === 'function') {
                    isFnct.push($(this).data().fnct[item][push])
                    push++
                }
                isFnct.push(_fnct)
                fnct[item] = isFnct
            } else {
                fnct[item] = $(this).data().fnct[item]
            }
        }
        if (push == 0) {
            fnct[motif] = [_fnct]
        }
        _this.data('fnct', fnct)
        return _this
    }

    $.fn.dataVar = function (_var = null, modifier = false) {
        var _this = $(this), __var = $(this).data._var || {}, isVar = [], push = 0;
        if (typeof _var === 'object' && _var !== null) {
            for (var key in _var) {
                for (var key2 in _var[key]) {

                    if (__var[key] && __var[key][key2] && !modifier)
                        continue;
                    else if (typeof __var[key] === 'undefined') {
                        __var[key] = {};
                        __var[key][key2] = _var[key][key2]
                    }
                }
            }
            $(this).data('_var', __var);
            return _this;
        } else {
            return $(this).data()._var;
        }

    }

    try {
        $('<style> hr[class^="space-"]{display: inline-block;height: 1px;padding: 0; margin: 0;border: 0px; background: #fdfeff00;}</style>').appendTo($('head'))
        $("[class^='space-']").each(function () {
            $(this).css('width', ($(this).attr('class')).split('-')[1])
        })
    } catch (e) {
        console.log(e)
    }


    $.fn.hasAttr = function (attribute) {
        //console.log($(this))
        if (typeof $(this)[0] === 'object') {
            //console.log($(this).attr(attribute))
            return (typeof $(this).attr(attribute) === 'string')
        }
        return false
    }

    $.fn.dettachClass = function (clss) {
        if ($(this).hasClass(clss)) {
            $(this).removeClass(clss);
            return this;
        }
        return this;
    }

    $.fn.outerCss = function (returns) {
        if (typeof returns === 'string') {
            if (returns === 'this')
                return {
                    left: parseInt($(this).css('padding-left')) + parseInt($(this).css('margin-left')),
                    right: parseInt($(this).css('padding-right')) + parseInt($(this).css('margin-right')),
                    top: parseInt($(this).css('padding-top')) + parseInt($(this).css('margin-top')),
                    bottom: parseInt($(this).css('padding-bottom')) + parseInt($(this).css('margin-bottom'))
                }
            else if (returns === 'parent') {
                var p = $(this).parent(), left = 0, top = 0, eps_left = parseInt($(this).offset().left) - parseInt(p.offset().left), eps_top = parseInt($(this).offset().top) - parseInt(p.offset().top)
                do {
                    if (p.css('position') == 'absolute' || p.css('position') == 'relative' || p.is('body'))
                        break;
                    eps_top = parseInt($(this).offset().top) - parseInt(p.offset().top)//new enter
                    eps_left = parseInt($(this).offset().left) - parseInt(p.offset().left)//new enter
                    p = p.parent();
                    left = left + parseInt(p.css('padding-left'));
                    top = top + parseInt(p.css('padding-top'));
                } while (p.css('position') != 'absolute' && p.css('position') != 'relative' && !p.is('body'));
                return {
                    /*'left': eps_left < 0 ? eps_left : eps_left + left,
                     'top': eps_top < 0 ? eps_top : eps_top + top*/
                    'left': eps_left, //new enter
                    'top': eps_top//new enter
                }
            } else
                return {
                    left: parseInt($(this).parent().parent().css('padding-left')) + parseInt($(this).parent().css('padding-left')) + parseInt($(this).css('padding-left')) + parseInt($(this).css('margin-left')),
                    right: parseInt($(this).parent().parent().css('padding-right')) + parseInt($(this).parent().css('padding-right')) + parseInt($(this).css('padding-right')) + parseInt($(this).css('margin-right')),
                    top: parseInt($(this).parent().parent().css('padding-top')) + parseInt($(this).parent().css('padding-top')) + parseInt($(this).css('padding-top')) + parseInt($(this).css('margin-top')),
                    bottom: parseInt($(this).parent().parent().css('padding-bottom')) + parseInt($(this).parent().css('padding-bottom')) + parseInt($(this).css('padding-bottom')) + parseInt($(this).css('margin-bottom'))
                }
        }
    }

    $.fn.generateChars = function (length) {
        var chars = '';
        for (var i = 0; i < length; i++) {
            var randomChar = Math.floor(Math.random() * 36);
            chars += randomChar.toString(36);
        }

        return chars;
    };
    $.fn.InputLoadRequest = function (option) {
        var _l = 'load-' + $(this).generateChars(4), self = $(this)

        $('<i/>').attr({
            'class': 'fa fa-spinner',
            //'loading': _l
        }).css({
            'position': 'relative', //'position': 'absolute',
            //'top': '69%',
            //'left': '50%',
            //'margin-left': '-15px',
            'margin-top': (((parseInt($(this).outerHeight()) - 22) / 2) < 0 ? 0 : ((parseInt($(this).outerHeight()) - 22) / 2)) + 'px', //'margin-top': '-15px',
            'color': 'rgb(36, 216, 83)',
            'font-size': '22px',
            '-webkit-animation': 'fa-spin 2s infinite linear',
            'animation': 'fa-spin 2s infinite linear'
        }
        ).appendTo($('<div/>').attr({
            'loading': _l
        }).css({
            'position': 'absolute',
            'text-align': 'center',
            'margin-top': (-1 * parseInt(self.outerHeight())) + 'px',
            //'top': parseInt($(this).offset().top),
            'left': (option && option.left) ? option.left : a = function () {
                return self.outerCss('parent').left
            },
            'width': self.outerWidth(),
            'height': self.outerHeight(),
            'background-color': '#868b90bd'
        }).appendTo(self.parent()))

        return {
            self: $('div[loading="' + _l + '"]'),
            remove: function () {
                $('div[loading="' + _l + '"]').remove()
            }
        }
    }

    try {
        $('.toggle-btn').click(function () {
            $('.' + $(this).attr('data-toggle')).show();
        });
    } catch (exception) {

    }
    try {
        $('.auto-hide').mouseleave(function () {
            $(this).hide();
        });
    } catch (exception) {

    }


    /*$.fn.parentOperation = function ($this) {
     var b_$this = $this.width();
     var h_$this = $this.height();
     var el_$this = $this.children().length;
     return {a: b_$this, b: h_$this, c: el_$this};
     }*/


    try {
        $('.info-cache').show(function () {
            $(this).hide();
        });
    } catch (exception) {

    }
    try {
        $this = $('.btn-remove');
        $parent = $this.parents('.box-removable');
    } catch (exception) {

    }

    try {
        $('.info-cache').mouseleave(function () {
            $(this).hide('slow');
        });
    } catch (exception) {

    }


    $('.panel-table-list-content').css({
        //'width': $('.panel-table-list').width()
        'width': parseInt(window.innerWidth) - 50
    })

    $('[data-toggle="collapse"]').on('rendreWidth', function (event) {
        $('.panel-table-list-content').css({
            //'width': $('.panel-table-list').width()
            'width': parseInt(window.innerWidth) - 50
        })
    })

    $('.form-group label').show(function () {
        $("input[id='" + $(this).attr('for') + "'").attr('placeholder', $(this).html());
    })

    $.fn.addTitle_old = function (cl_css = 'title-info') {
        $_ele_ = $(this).has('[title]')
        if ($_ele_)
            return $(this).show(function () {

                $("<span/ > ").html($(this).attr('title')).appendTo($(this)).attr({
                    'class': 'title ' + cl_css
                })
                $(this).removeAttr('title').hover(function () {
                    $(this).children('.title').css('visibility', 'visible')
                }).mouseleave(function () {
                    //$(this).children('.title').css('visibility', 'hidden')
                })

            }
            )
    }

    $.fn.addTitle = function (cl_css = 'title-info') {
        var el = $(this)
        if ($(this).hasAttr('title-old') && !$(this).hasAttr('title_'))
            return this
        if ($(this).hasAttr('title') || $(this).hasAttr('title_'))
            $(this).off('mouseenter')
        $(this).mouseenter(function (event) {
            var _title_ = $(this).attr('title')
            $(this).attr('title-old', _title_)
            var _sess_ = 'info-title-' + new Date().getTime()
            $(this).removeAttr('title')
            var x = $(this).offset()
            var _this_ = $("<span/ > ").html(_title_).appendTo('body').attr({
                'class': 'title ' + cl_css,
                'sess-actif': _sess_
            })
            _this_.css({
                'visibility': 'visible',
                'z-index': '100000',
                'right': 'unset',
                'top': parseInt(event.pageY) - (parseInt(_this_.height()) / 2) + 'px',
                'left': parseInt(event.pageX) - (parseInt(_this_.width()) + 36) + 'px'
            })
            //console.log(event.clientY,event.pageY,Math.round($(window).scrollTop()))
            $(this).mouseleave(function () {
                try {
                    $("[sess-actif='" + _sess_ + "']").remove()
                } catch (e) {
                }

                $("[sess-actif^='info-title-']").each(function () {
                    $(this).remove()
                })
                if ($(this).hasAttr('title-old')) {
                    $(this).attr('title', $(this).attr('title-old'))
                    $(this).removeAttr('title-old')
                } else if (!$(this).hasAttr('title'))
                    $(this).off('mouseenter')
            })
        }
        )
        return el
    }

    $('[title]').addTitle()

    $(window).resize(function () {
        // alert(window.innerHeight);
       

        $('.bg-image').each(function () {
            $(this).css({
                'background-size': $(this).width() + 42 + 'px ' + ($(this).height() + (scr_h / $(this).height() * 100)) + 'px'
            });
        })

    });
    /*$.fn._form = function () {
     return {
     isValid:this.context.validity.valid,
     error:this.context.validationMessage
     }
     }*/

    $.fn.controleInput = function () {
        var element = $(this), dd = function (val) {

            var i = val.split(' '), resulta = val
            for (var j = 0; j < i.length; j++) {
                resulta = j == 0
                    ?
                    $.trim(i[j]) !== '' ? $.trim(i[j]).substr(0, 1).toUpperCase() + $.trim(i[j]).substr(1) : ''
                    :
                    (resulta !== ''
                        ?
                        resulta += ' ' + ($.trim(i[j]) !== '' ? $.trim(i[j]).substr(0, 1).toUpperCase() + $.trim(i[j]).substr(1) : '')
                        :
                        $.trim(i[j]) !== '' ? $.trim(i[j]).substr(0, 1).toUpperCase() + $.trim(i[j]).substr(1) : '')
            }
            return $.trim(resulta)
        }

        element.each(function () {
            $(this).focusout(function (e) {
                if ($(this).is('input[type="email"]') || $(this).is('input[type="password"]') || $(this).hasAttr('custom')) {
                    $(this).val(trimPoint(e.target.value))
                    return;
                }
                if ($(this).hasClass('upercase'))
                    $(this).val(e.target.value.toUpperCase())
                else
                    $(this).val(dd(trimPoint(e.target.value)))
            })
            $(this).focusin(function () {
                $(this).select()
            })
            if ($(this).is('[type="checkbox"]'))
                $(this).click(function (e) {
                    $(this).val(e.target.checked ? 1 : 0);
                })
            if ($(this).is('input[type="checkbox"]'))
                $(this).val($(this)[0].checked ? 1 : 0);
        })
        return $(this)
    }
    $('input,textarea').controleInput();
    $.fn._form = {
        isValid: function (form) {
            _true_ = 1
            $(form).find('input,select,textarea').each(function () {
                if ($(this).is('input') || $(this).is('select') || $(this).is('textarea')) {
                    if ($(this).context.validity.valid) {
                        //console.log($(this).nextAll("i[class*='fa-warning']"))
                        if ($(this).nextAll("i[class*='fa-warning']").length)
                            $(this).nextAll("i[class*='fa-warning']").each(function () {
                                $(this).remove()
                            })
                    } else {
                        if ($(this).nextAll("i[class*='fa-warning']").length)
                            $(this).nextAll("i[class*='fa-warning']").each(function () {
                                $(this).remove()
                            })
                        $(this).after($('<i/>').attr({
                            'id': '#_00',
                            'class': 'fa fa-warning',
                            'title': $(this)[0].validationMessage
                        }).css({
                            'position': 'absolute',
                            'top': '9px',
                            'right': '16px',
                            'color': 'red',
                            'z-index': '1'
                        }).addTitle('title-warning'))
                        _true_ = 0
                    }
                }
            })

            return _true_
        },
        news: function (form) {
            $(form).find('input, select, textarea').each(function () {
                //console.log($(this).is('textarea'))
                if ($(this).is(':text') || $(this).is('[type="date"]') || $(this).is('[type="email"]') || $(this).is('select') || $(this).is('textarea')) {
                    $(this).val('')//.attr('title', $(this)[0].validationMessage).addTitle()
                }
                if ($(this).nextAll("i[class*='fa-warning']").length)
                    $(this).nextAll("i[class*='fa-warning']").each(function () {
                        $(this).remove()
                    })
                if ($(this).is('input[type="checkbox"]')) {
                    $(this)[0].checked = false;
                    $(this).val($(this)[0].checked ? 1 : 0);
                }

            })

            $(form).find('span[data-id-mySelected]').each(function () {
                if ($(this).is('span')) {
                    $(this).attr('title', 'Veuillez selectioner une valeur').html('Sélectionnez ').addTitle()
                }
                if ($(this).nextAll("i[class*='fa-warning']").length)
                    $(this).nextAll("i[class*='fa-warning']").each(function () {
                        $(this).remove()
                    })

            })
            $('form').trigger('new', [form])
            return this
        },
        setForm: function (form, data) {
            if (typeof data === 'object' && $(form).is('form')) {
                for (x in data) {
                    var $_input_ = $(form).find('input[name="' + x + '"],select[name="' + x + '"],textarea[name="' + x + '"]')
                    console.log($($_input_).is('input[name="' + x + '"]'))
                    if (!$($_input_).is('input[type="checkbox"]') && ($($_input_).is('input[name="' + x + '"]') || $($_input_).is('select[name="' + x + '"]') || $($_input_).is('textarea[name="' + x + '"]'))) {
                        $_input_.val((typeof data[x] !== 'undefined' && data[x] !== null) ? '' + data[x] + ''.trim() : '')
                        if ($_input_.hasAttr('data-selected')) {
                            $('[data-id-myselected="' + $_input_.attr('data-selected') + '"]').html($_input_.val())
                        }
                    }
                    if ($($_input_).is('input[type="checkbox"]')) {
                        $($_input_)[0].checked = (data[x] && parseInt('' + data[x] + ''.trim()) === 1) ? true : false;
                        $($_input_)[0].value = '' + data[x] + ''.trim();
                    }
                }
                if (form.hasAttr('data-update') && !form.find('input[name="UPDATA"]').length) {
                    $('<input/>', { name: 'UPDATE', hidden: 'hidden', value: data[form.attr('data-update')] }).appendTo(form)
                }
            }
            return form;
        }

    }

    $.fn.mySerialize = function () {
        var data = {};
        $(this).find('input[name],textarea[name],select[name]').each(function () {
            data[$(this)[0].name] = $(this)[0].value;
        })
        return data
    }

    $.fn._formError = function () {
        $_this = $(this)
        return {
            serveur: function (data) {
                if (data) {
                    console.log(data)
                    console.log(data.responseJSON.errors)
                    console.log(typeof data.responseJSON.errors)
                    if (typeof data.responseJSON.errors === 'object')
                        console.log('object')
                    for (x in data.responseJSON.errors) {
                        console.log(typeof data.responseJSON.errors[x])
                        if (typeof data.responseJSON.errors[x] === 'object') {
                            console.log('object')
                            $_input_ = $($_this).find('input[name="' + x + '"], select[name="' + x + '"], textarea[name="' + x + '"]')
                            if ($($_input_).hasAttr('name') && typeof data.responseJSON.errors[x][0] === 'string') {
                                console.log('string')
                                if ($($_input_).nextAll("i[class*='fa-warning']").length)
                                    $($_input_).nextAll("i[class*='fa-warning']").each(function () {
                                        $(this).remove()
                                    })
                                $($_input_).after($('<i/>').attr({
                                    'id': '#_00',
                                    'class': 'fa fa-warning',
                                    'title': data.responseJSON.errors[x][0]
                                }).css({
                                    'position': 'absolute',
                                    'top': '9px',
                                    'right': '16px',
                                    'color': 'red',
                                    'z-index': '1'
                                }).addTitle('title-warning'))
                            }
                        }

                    }


                }
            }
        }
    }

    $.fn.optionConfirmation = function (title = 'Title', action) {
        var _i_ = ')]/' + $(this).generateChars(4), element = $(this)
        var modal = $('<div/>', {
            'class': 'row justify-content-center MyModal'
        }).append(
            $('<div/>', {
                'class': 'panel panel-default',
                'tabmodal': _i_
            }).clone(true)),
            header = $('<div/>', {
                'class': 'panel-heading',
                'style': 'text-align:center;'
            }).html(' ' + title),
            footer = $('<div/>', {
                'class': 'panel-footer text-right'
            }).append(
                $('<button/>', { 'type': 'button', 'class': 'btn btn-info', 'style': 'float:left;' }).click(function () {
                    if (typeof action === 'function')
                        action.apply(this, [modal, false])
                    modal.remove()
                }).html('Annuler').clone(true)
            ).append(
                $('<button/>', { 'type': 'button', 'class': 'btn btn-primary', 'style': 'background-color: #e3342f80;' }).click(function () {
                    if (typeof action === 'function')
                        action.apply(this, [modal, true])
                }).html('Ok').clone(true)
            )

        header.appendTo(modal.children())
        footer.appendTo(modal.children())
        //console.log(modal)
        modal.appendTo('body')
        $('<p/>', {
            'data-modal': _i_
        }).myModal()


    }


    $.fn.monPOST = function (methode = null, even, action) {
        var _this_ = $(this)
        if (!_this_.is('form') && typeof action === 'string')
            _this_ = $('<form/>', {
                'action': action
            }).append($('<input/>', {
                'name': 'form'
            }).clone())
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token-x"]').attr('content')
            },
            url: base_url + _this_.attr('action'),
            data: _this_.mySerialize(),
            callback: _this_.data('fnct'),
            listner: 'ajouter',
            type: 'POST',
            success: function (_data_, textStatus, jqXHR) {
                if (methode) {
                    var _data__ = { success: _data_, error: null }
                    methode(_data__, textStatus, jqXHR);
                }

            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (methode) {
                    var _data_ = { success: null, error: jqXHR }
                    methode(_data_, textStatus, jqXHR);
                }
            },
            complete: function (jqXHR, textStatus) {
                if (even && typeof even === 'function')
                    even()
            }
        });
    }

    /*
     * 
     * @param {type} methode
     * @param {type} data
     * @param {type} even
     * @param {type} action
     * @returns {globaleL#101.$.fn}
     */
    $.fn.customerPOST = function (methode = null, data = null, even, action) {
        var _this_ = $(this)
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token-x"]').attr('content')
            },
            url: action.search("://") > 0 ? action : (base_url + action),
            data: data,
            type: 'POST',
            success: function (_data_, textStatus, jqXHR) {
                if (methode) {
                    var _data__ = { success: _data_, error: null }
                    methode(_data__, textStatus, jqXHR);
                }

            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (methode) {
                    var _data_ = { success: null, error: jqXHR }
                    methode(_data_, textStatus, jqXHR);
                }
            },
            complete: function (jqXHR, textStatus) {
                if (even && typeof even === 'function')
                    even()
            }
        });
        return this
    }

    $.fn.monGET = function (action, data = null, methode = null, even = null) {
        var _this_ = $(this)
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: base_url + action,
            data: data,
            type: 'GET',
            success: function (_data_, textStatus, jqXHR) {
                if (methode) {
                    _data_ = { success: _data_, error: null }
                    methode(_data_, textStatus, jqXHR);
                }

            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (methode) {
                    var _data_ = { success: null, error: jqXHR }
                    methode(_data_, textStatus, jqXHR);
                }
            },
            complete: function (jqXHR, textStatus) {
                if (even && typeof even === 'function')
                    even()
            }
        });
    }

    $.fn.monDELETE = function (methode = null, even, action) {
        var _this_ = $(this)
        if (!_this_.is('form') && typeof action === 'string')
            _this_ = $('<form/>', {
                'action': action
            }).append($('<input/>', {
                'name': 'form'
            }).clone())
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token-x"]').attr('content')
            },
            url: base_url + _this_.attr('action') + '/delete',
            data: _this_.mySerialize(),
            callback: _this_.data('fnct'),
            listner: 'delete',
            type: 'DELETE',
            success: function (_data_, textStatus, jqXHR) {
                if (methode) {
                    var _data__ = { success: _data_, error: null }
                    methode(_data__, textStatus, jqXHR);
                }

            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (methode) {
                    var _data_ = { success: null, error: jqXHR }
                    methode(_data_, textStatus, jqXHR);
                }
            },
            complete: function (jqXHR, textStatus) {
                if (even && typeof even === 'function')
                    even()
            }
        });
    }

    $.fn.monUPDATE = function (methode = null, even, action) {
        var _this_ = $(this)
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token-x"]').attr('content')
            },
            url: base_url + _this_.attr('action') + '/update',
            data: _this_.mySerialize(),
            callback: _this_.data('fnct'),
            listner: 'modifier',
            type: 'PUT',
            success: function (_data_, textStatus, jqXHR) {
                if (methode) {
                    var _data__ = { success: _data_, error: null }
                    methode(_data__, textStatus, jqXHR);
                }

            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (methode) {
                    var _data_ = { success: null, error: jqXHR }
                    methode(_data_, textStatus, jqXHR);
                }
            },
            complete: function (jqXHR, textStatus) {
                if (even && typeof even === 'function')
                    even()
            }
        });
    }

    $.fn.monUploadImage = function (methode = null, evenStrat, evenStop) {
        $(this).each(function () {
            $(this).change(function () {
                var event = {}
                if (evenStrat && typeof evenStrat === 'function')
                    evenStrat(event)
                var element = $(this), el_ = this, form = new FormData();
                form.append('file', el_.files[0]);
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token-x"]').attr('content')
                    },
                    url: base_url + element.attr('action'),
                    data: form,
                    cache: true,
                    contentType: false,
                    processData: false,
                    callback: element.data('fnct'),
                    type: 'POST',
                    success: function (_data_, textStatus, jqXHR) {
                        if (methode) {
                            var _data__ = { success: _data_, error: null }
                            methode(_data__, textStatus, jqXHR);
                        }

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        if (methode) {
                            var _data_ = { success: null, error: jqXHR }
                            methode(_data_, textStatus, jqXHR);
                        }
                    },
                    complete: function (jqXHR, textStatus) {
                        if (evenStop && typeof evenStop === 'function')
                            evenStop(event)
                    }
                });
            })
        })
    }

    $.fn.myModalRequest = function (methode = null) { //data-request
        var _this_ = $(this)
        //console.log(_this_.attr('data-request'))
        var _data_ = { success: null, error: null }
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: base_url + _this_.attr('data-action'),
            data: { 'modifier': _this_.attr('data-request') },
            type: 'GET',
            success: function (_data_, textStatus, jqXHR) {
                if (methode) {
                    _data_ = { success: _data_, error: null }
                    methode(_data_, textStatus, jqXHR);
                }


            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (methode) {
                    _data_ = { success: null, error: jqXHR }
                    methode(_data_, textStatus, jqXHR);
                }
            }
        });
    }


    $('[data-toggle="tooltip"]').tooltip();
    DisplayInfo = function (display, _block = false) {
        var ol = $('<ol/>'), li = function () {
            return $('<li/>')
        }, dl = function () {
            return $('<dl/>', { 'class': 'dl-horizontal info-liste' })
        }, block = function () {
            return $('<div/>', { 'class': 'block-info-liste' })
        }
        var i = 0
        while (display && display[i] && _block) {
            if (typeof display[i].reference === 'string') {
                var head = new li(), body = new dl(), _b = new block()
                head.html(display[i].reference)
                for (var k = 0; k < display[i].content.length; k++) {
                    if (typeof display[i].content[k] === 'object') {
                        body.append($('<dt/>').html(display[i].content[k].title).clone(true)).append($('<dd/>').html('<b>:</b> ' + display[i].content[k].define).clone(true))
                    }
                }
                head.appendTo(_b)
                body.appendTo(_b)
            } else
                i = display.length + 1;
            _b.appendTo(ol)
            i++
        }

        while (display && display[i] && !_block) {
            if (typeof display[i].reference === 'string') {
                var head = new li()
                head.html(display[i].reference)
                head.appendTo(ol)
                for (var k = 0; k < display[i].content.length; k++) {
                    if (typeof display[i].content[k] === 'object') {
                        var body = new dl(), _b = new block()
                        body.append($('<dt/>').html(display[i].content[k].title).clone(true)).append($('<dd/>').html('<b>:</b> ' + display[i].content[k].define).clone(true))
                        body.appendTo(_b)
                        _b.appendTo(ol)
                    }
                }

            } else
                i = display.length + 1;
            i++
        }
        return display && i == display.length ? ol : ''
    }

   

    /*******************************************************************************************************/

    $('[form="post"]').each(function () {
        $(this).click(function (e) {
            var self = $(this), form, elem = self, ev = self.InputLoadRequest()
            do {
                elem = elem.parent()
                form = elem.find('form')
            } while (form.length == 0 && !elem.is('body'))
            var request_fnt = function (_data_, textStatus, jqXHR) {
                if (!_data_.error) {
                    $(this)._form.news();
                } else {
                    $(form)._formError().serveur(_data_.error)
                }
            }
            if (form.length && $(this)._form.isValid(form)) {
                form.monPOST(request_fnt,
                    function () {
                        ev.remove()
                    }
                );
            } else
                ev.remove()

        })
    })


    $('[form="delete"]').each(function () {
        $(this).click(function (e) {
            var self = $(this), form, elem = self, ev = self.InputLoadRequest()//{left: 'unset'}
            do {
                elem = elem.parent()
                form = elem.find('form')
            } while (form.length == 0 && !elem.is('body'))
            if (form.length)
                $(this).optionConfirmation('Voulez-vous supprimer cette information ?', function (modal, action) {
                    if (action) {
                        form.monDELETE(
                            function (data__, textStatus, jqXHR) {
                                if (data__.success) {
                                    /* $(this)._form.news(form)
                                     if (self.parents('div[tabmodal]').length)
                                     $('<p/>', {
                                     'data-dismiss': self.parents('div[tabmodal]').attr('tabmodal')
                                     }).myModal('hide')
                                     */
                                }
                            },
                            function () {
                                ev.remove()
                            }
                        )
                        modal.remove()
                    } else
                        ev.remove()
                })
            else
                ev.remove()

        })
    })

    $('[form="update"]').each(function () {
        $(this).click(function (e) {
            var self = $(this), form, elem = self; //{left: 'unset'}
            do {
                elem = elem.parent();
                form = elem.find('form');
            } while (form.length == 0 && !elem.is('body'))
            if (form.length)
                $(this).optionConfirmation('Voulez-vous supprimer cette information ?', function (modal, action) {
                    if (action) {
                        if (self._form.isValid(form)) {
                            var ev = self.InputLoadRequest(), request_fnt = function (_data_, textStatus, jqXHR) {
                                if (_data_.error) {
                                    form._formError().serveur(_data_.error)
                                }
                            };
                            form.monUPDATE(request_fnt,
                                function () {
                                    ev.remove()
                                }
                            );
                        }
                        modal.remove()
                    } else
                        ev.remove()
                })
            else
                ev.remove()
        })
    })


    $('[panel-btn-action]').show(function () {
        $('head').append('<style>.panel-action.active{padding: 5px;} .panel-action.active, .panel-action.active .content-action,.panel-action.active .content-action *{height:auto !important; } .panel-action, .panel-action .content-action,.panel-action .content-action *{height:0px; } .panel-action{height: 0px;position: absolute;border: 2px solid #ddd;border-radius: 0px 0px 20px 20px;border-top-color: white;background: #9eb9d1ed;border-top: 0px;border-right: 0px;border-left: 0px;padding: 0px;text-align: center;}</style>')
        $(this).each(function () {
            var el_panel_btn = $(this), parent = el_panel_btn.parent()
            //console.log((parseInt(parent.width()) - parseInt(el_panel_btn.width())) / 2,parent,el_panel_btn,parent.width(),el_panel_btn.innerWidth())
            el_panel_btn.css('width', parent.width())
            listner_resize.addEvent(el_panel_btn, parent, function (k, j) {
                k.css('width', j.width())
            })
            $(this).addClass('panel-action').append($('<i/>', {
                'class': 'fa fa-fw fa-unsorted',
                'style': 'display:block;width:100%;margin-bottom:-7px;height: 39px;position:absolute;color: #8c8ca2;'
            }).click(function () {
                if ($(this).hasClass('down')) {
                    el_panel_btn.removeClass('active')
                    $(this).removeClass('down')
                } else {
                    el_panel_btn.addClass('active')
                    $(this).addClass('down')
                }
            }).clone(true)).mouseleave(function () {
                if ($(this).children('i').hasClass('down')) {
                    $(this).removeClass('active')
                    $(this).children('i').removeClass('down')
                }
            })
        })
    })

}
)
    ;


$('.panel-footer.submit').each(function () {
    var $_0 = $('<a/>').attr({
        'href': '#'
    }).appendTo($(this));

    var $_1 = $('<span/>').attr({
        'id': '#_1',
        'class': 'pull-left'
    }).html("Valider les informations").appendTo($_0);
    var $_2 = $('<span/>').attr({
        'id': '#_2',
        'class': 'pull-right'
    });
});

/*
 .timeline:before {
 content: '';
 position: absolute;
 top: 0;
 bottom: 0;
 width: 4px;
 background: #ddd;
 left: 31px;
 margin: 0;
 border-radius: 2px;
 }
 */
/*
 <div style="
 height: 0px;width: max-content;position: absolute;margin-left: 33%;border: 1px solid black;border-radius: 0px 0px 20px 20px;border-top-color: white;
 ">
 <div class="content-action" style="
 height: 0px;
 ">
 <a class="btn btn-block-inline btn-social btn-add" style="margin-left: 20px;" data-modal="ajouter_conger">
 <i class="fa fa-plus-circle"></i> Ajouter congé
 </a>
 </div>
 
 <i class="fa fa-fw fa-unsorted" style="
 display: block;
 margin-left: 50%;
 margin-bottom: -7px;
 "></i>
 
 </div>
 
 .content-action * {
 height: 0px;
 }
 */

/*
 * 
 * 
 * $.extend( jQuery.expr[':'], {
 largeur : function(a, i, m){
 return $(a).css('width') == m[3]; // retourne les éléments
 ayant la largeur donnée
 }
 } );
 // exemple :
 $('elements:largeur("100px")'); // sélectionne seulement les
 éléments ayant une largeur de 100px exactement
 */

/*
 * adress [0,1]['':{title,info}]
 * notice [0,n]string
 * head {ID{title}}
 * body [0,n]{ID!}
 * resultat [0,n]{ID!}
 * resumer [0,n]{title, info}
 * observation [Array|String]
 */
var GenererFichePaie = function (el, calcule) {
    var fiche = $('<fieldset/>', {
        'style': 'display: block;margin-inline-start: 2px;margin-inline-end: 2px;padding-block-start: 0.35em;padding-inline-start: 0.75em;padding-inline-end: 0.75em;padding-block-end: 0.625em;min-inline-size: min-content;border-width: 2px;border-style: groove;border-color: threedface;border-image: initial;'
    }), feuille = $.extend(true, {}, { title: 'Notre fiche', address: null, notice: null, header: null, body: null, resultat: null, resumer: null, observation: null }, calcule || {}),
        countColoums = 0,
        adress = function () {
            var style = 'border-width: 1px;border-style: groove;border-color: white;border-image: initial;padding: 17px;width: 330px;display: inline-block;text-align : justify;font-family: "Times New Roman", Times, serif;font-style : normal;';
            return $('<address/>', { 'style': style });
        }, adressDiv = function () {
            return $('<div/>', { 'style': 'padding: 0px;border: 0px;' });
        }, ligneSpace = function (space = '10') {
            return $('<hr/>', {
                'style': 'margin: 0px;width: ' + space + 'px;display: inline-block;height: 0px;border: 0px;'
            });
        }, notice = function (msg = '') {
            return $('<br><small style="display: inline-block;">' + msg + '</small>');
        }, ficheCalc = function () {
            return $('<table/>', {
                'class': 'table table-condensed',
                'style': 'margin-top: 10px;'
            });
        }, row = function () {
            return $('<tr/>');
        }, coloneHead = function (html) {
            return $('<th/>', {
                'style': 'border-left: 1px solid;border-right: 1px solid;'
            }).html(html)
        }, coloneBody = function (html) {
            return $('<td/>', {
                'style': 'border-left: 1px solid;border-right: 1px solid;'
            }).html(html);
        }, breakLigne = function () {
            return $('<td colspan="' + countColoums + '"></td>');
        }, coloneResult = function (html) {
            return $('<td/>').html(html);
        }, resumer = function (data) {
            var x = $('<td colspan="' + countColoums + '" style="border:1px solid black;"></td>'), i = 0;
            while (data[i]) {
                $('<small style="display: inline-block;width: 47%;font-size: 11px;text-align: right;padding-left: 30px;"><span style="float: left;font-size: 11px;">' + data[i]['title'] + ' :</span> ' + data[i]['info'] + '</small>').appendTo(x);
                i++
            }
            return x;
        }, observation = function (obs) {
            if (typeof obs === 'object')
                return '<u>OBSERVATION</u> : <small style="text-font:12px;">' + obs.join(',<hr style="margin: 0px;width: 10px;display: inline-block;height: 0px;border: 0px;">') + '</small>';
            return '<u>OBSERVATION</u> : <small style="text-font:12px;">' + obs + '</small>';
        }
    fiche.append($('<legend/>', {
        'style': 'width: 100%;display: block;padding-inline-start: 2px;padding-inline-end: 2px;border-width: initial;border-style: none;border-color: initial;border-image: initial;'
    }).html(feuille.title).clone(true))

    /*  header  */

    for (var adress_ = 0; feuille.address && adress_ < feuille.address.length; adress_++) {
        var addressCont = new adress().css('float', (adress_ % 2 === 1) ? 'right' : 'left'), yu = 1, placer = false, div = new adressDiv();
        for (var detail in feuille.address[adress_]) {
            placer = false;
            div.append($('<b/>').html(detail + ': ').clone(true)).append(feuille.address[adress_][detail]).append(new ligneSpace().clone(true));
            if (yu > 0 && yu % 2 === 0) {
                addressCont.append(div.clone(true));
                div = new adressDiv();
                placer = true;
            }
            yu++;
        }
        if (!placer)
            addressCont.append(div.clone(true));
        fiche.append(addressCont.clone(true))
    }
    if (feuille.notice && typeof feuille.notice === 'object')
        for (var item_notice in feuille.notice) {
            if (typeof feuille.notice[item_notice] === 'string')
                fiche.append(new notice(feuille.notice[item_notice]));
        }
    else if (feuille.notice && typeof feuille.notice === 'string')
        fiche.append(new notice(feuille.notice));
    /* body */
    if (feuille.header === null) {
        el.html('').append($('<h1/>', { 'style': 'margin: 180px;text-align:center;color:brown;' }).html('Vous n\'avez pas de données sélectionner'))
        return el;
    }

    var table = new ficheCalc(), rowHead = new row(), headId = [], thead = $('<thead/>', { 'style': 'border: 1px solid;' }), tbody = $('<tbody/>'), tfoot = $('<tfoot/>'), parcBody = 0, parcResult = 0;
    for (var _item in feuille.header) {//{ID{title}}
        headId.push(_item)
        rowHead.append(new coloneHead(feuille.header[_item]['title']).clone(true));
        countColoums++
    }
    table.append(thead.append(rowHead.clone(true)).clone(true));
    while (feuille.body && feuille.body[parcBody]) {
        for (var i = 0, rowBody = new row(); i < headId.length; i++) {
            rowBody.append(new coloneBody(feuille.body[parcBody][headId[i]]).clone(true));
        }
        parcBody++;
        tbody.append(rowBody.clone(true));
    }
    table.append(tbody.append(new breakLigne().css('border-top-color', 'black')).clone(true))
    while (feuille.resultat && feuille.resultat[parcResult]) {
        for (var i = 0, rowResult = new row(); i < headId.length; i++) {
            rowResult.append(new coloneResult(feuille.resultat[parcResult][headId[i]]).clone(true));
        }
        parcResult++;
        tfoot.append(rowResult.clone(true));
    }
    if (feuille.resumer && typeof feuille.resumer === 'object')
        tfoot.append(new breakLigne()).append(new row().append(new resumer(feuille.resumer).clone(true)).clone(true));
    fiche.append(table.append(tfoot.clone(true)).clone(true));
    if (feuille.observation)
        fiche.append(observation(feuille.observation));
    el.html('').append(fiche.clone(true));
    return el;
}



