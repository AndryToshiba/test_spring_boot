/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function (e) {

    $('<style> .MyModal .panel .panel-heading{font: normal normal normal 14px/1 FontAwesome;}</style>').appendTo($('head'));
    $(window).resize(function () {
        $('.MyModal .panel.max').css({
            'width': window.innerWidth - 100 + 'px'
        })
        $('.MyModal .panel.min').css({
            'width': window.innerWidth - 250 + 'px'
        })
    })

    /*$(window).scroll(function () {
     $('.MyModal .panel').each(function () {
     $(this).css({
     'top': 100 + $(window).scrollTop(),
     ' -webkit-transition': 'width 2s', // Safari 
     'transition': 'width 2s'
     })
     })
     })*/


    $('.MyModal .panel.max').on('open', function () {
        $(this).css({
            'width': window.innerWidth - 100 + 'px'
        })
    }).bind('show', function (e) {
        $(this).css({
            'width': window.innerWidth - 100 + 'px'
        })
    })

    $('.MyModal .panel.min').on('open', function () {
        $(this).css({
            'width': window.innerWidth - 250 + 'px'
        })
    }).bind('show', function (e) {
        $(this).css({
            'width': window.innerWidth - 250 + 'px'
        })
    })


    try {
        $('.MyModal .panel').on('new-window', function (e) {// non encore utiliser
            $(this).css({
                'outline-width': screen.width,
                'display': $(this).css('display') != 'none' ? 'none' : 'none'
            })
        }).bind('show', function (e) {
            $(this).css({
                'outline-width': screen.width
            })
        });
    } catch (exception) {

    }


    $('[data-modal]').click(function () {

        try {
            $(this).myModal();
        } catch (exception) {

        }

    });
    $('[data-dismiss]').click(function () {
        try {
            $(this).myModal('hide');
        } catch (exception) {

        }
    });
    $.fn.hideModal = function (is, callback) {
        if (typeof is !== 'string')
            return $(this)
        $('<p/>', {
            'data-dismiss': is
        }).myModal('hide', callback)
        return $(this)
    }

    $.fn.showModal = function (is, callback) {
        if (typeof is !== 'string')
            return $(this)
        $('<p/>', {
            'data-modal': is
        }).myModal('show', callback)
        return $(this)
    }
    /*
     * 
     * @param {type} $_is
     * @param {type} callback ({modal: $_this, is: 'show'}, _data_.success)
     * @returns {$.fn|_$.fn.myModal.$_this|$.fn.myModal.$_this|$|_$}
     */
    $.fn.myModal = function ($_is = '', callback) {
//console.log($(this))
        var fadeFram = function (index) {
            return $('<div/>', {'fadeFram': 'show', 'style': 'position:fixed;top:0;right:0;bottom:0;left:0;z-index:' + index + ';display:block;overflow:hidden;-webkit-overflow-scrolling:touch;outline:0;'}).clone(true);
        }

        var $_bt_event = $(this)
        var $_isModal_index = 0;
        var $_isModal_index_next = 0;
        var $_this = $_is == 'hide'
                ?
                $("[tabmodal='" + $(this).attr('data-dismiss') + "']")
                :
                $("[tabmodal='" + $(this).attr('data-modal') + "']");
        if (!$_this)
            return this;
        try {
            $('[tabmodal]').each(function () {
                $_isModal_index = parseInt($(this).attr('tabindex')) > $_isModal_index ? parseInt($(this).attr('tabindex')) : $_isModal_index;
            })

        } catch (exception) {

        }

        switch ($_is) {
            case 'hide':
                if (parseInt($_this.attr('tabindex')) == $_isModal_index) {
                    $_this.removeAttr('tabindex').css({'display': 'none', 'opacity': 0}).prevAll('div[fadeFram]').each(function () {
                        $(this).remove();
                    });
                    if (typeof callback === 'function')
                        callback.call(this, {modal: $_this, is: 'hide'}, null)
                }

                break;
            default:
                if (!$_this.attr('tabindex')) {
                    $('.MyModal .panel.max').trigger('open')
                    $('.MyModal .panel.min').trigger('open')
                    if ($_this.find('form').length) {
                        var _req_form = $_this.find('form')[0]
                        if ($($_bt_event).hasAttr('_form_reponse'))
                            $(_req_form).attr('_form_request', $_bt_event.attr('_form_reponse'))
                        if ($(_req_form).is('form'))
                            $(this)._form.news(_req_form)
                        //console.log(_req_form)
                        //console.log(_req_form.hasAttribute('data-request'))
                        if (_req_form.hasAttribute('data-action')) {
//console.log(_req_form)
                            $(_req_form).myModalRequest(function (_data_, textStatus, jqXHR) {
                                if (!_data_.error) {
//console.log(_data_)
                                    $_this.attr('tabindex', $_isModal_index + 1).css({
                                        'z-index': 1000 + $_isModal_index + 1,
                                        'display': 'block',
                                        'opacity': 1,
                                        'outline-width': screen.width
                                    }).parent().prepend(new fadeFram(1000 + $_isModal_index + 1));
                                    $(this)._form.setForm($(_req_form), _data_.success)
                                    if (typeof callback === 'function')
                                        callback.call(this, {modal: $_this, is: 'show'}, _data_.success)

                                    //$('.MyModal .panel.max').trigger('open')
                                    //$('.MyModal .panel.min').trigger('open')

                                    /*if ($_bt_event.hasAttr('title_') && $_bt_event.hasAttr('title-old'))
                                     $_bt_event.attr('title-old', $_bt_event.attr('title_')).addTitle('title-info').removeAttr('title_')
                                     else if ($_bt_event.hasAttr('title') && $_bt_event.hasAttr('title_'))
                                     $_bt_event.attr('title', $_bt_event.attr('title_')).addTitle('title-info').removeAttr('title_')
                                     else if ($_bt_event.hasAttr('title-old') && $_bt_event.hasClass('request-error'))
                                     $_bt_event.removeAttr('title-old')
                                     else if ($_bt_event.hasAttr('title') && $_bt_event.hasClass('request-error'))
                                     $_bt_event.removeAttr('title')
                                     $_bt_event.removeClass('btn-danger request-error')
                                     */
                                } else {
                                    /*if ($_bt_event.hasAttr('title-old') && !$_bt_event.hasAttr('title_')) {
                                     $_bt_event.attr({
                                     'title_': $_bt_event.attr('title-old'),
                                     'title-old': bib_lang['bt_serveur_status'] + jqXHR['statusText']
                                     }).addTitle('title-warning')
                                     } else if ($_bt_event.hasAttr('title') && !$_bt_event.hasAttr('title_'))
                                     $_bt_event.attr({
                                     'title_': $_bt_event.attr('title'),
                                     'title': bib_lang['bt_serveur_status'] + jqXHR['statusText']
                                     }).addTitle('title-warning')
                                     else if ($_bt_event.hasAttr('title_') && $_bt_event.hasAttr('title'))
                                     $_bt_event.attr('title', bib_lang['bt_serveur_status'] + jqXHR['statusText'])
                                     else {
                                     $_bt_event.attr('title-old', bib_lang['bt_serveur_status'] + jqXHR['statusText'])
                                     }
                                     $_bt_event.addClass('btn-danger request-error')*/
                                }
                            })
                        } else {
                            $_this.attr('tabindex', $_isModal_index + 1).css(
                                    {
                                        'z-index': 1000 + $_isModal_index + 1,
                                        'display': 'block',
                                        'opacity': 1,
                                        'outline-width': screen.width
                                    }
                            ).parent().prepend(new fadeFram(1000 + $_isModal_index + 1));
                            if (typeof callback === 'function')
                                callback.call(this, {modal: $_this, is: 'show'}, null)
                        }
                    } else {
                        $_this.attr('tabindex', $_isModal_index + 1).css(
                                {
                                    'z-index': 1000 + $_isModal_index + 1,
                                    'display': 'block',
                                    'opacity': 1,
                                    'outline-width': scr_w
                                }
                        ).parent().prepend(new fadeFram(1000 + $_isModal_index + 1));
                        if (typeof callback === 'function')
                            callback.call(this, {modal: $_this, is: 'show'}, null)
                    }
                }
                break;
        }
        return $_this

    }


    $('.panel-footer :submit').click(function (e) {
        e.preventDefault()
        var $_footer = $(this).parents('.panel-footer'),
                $_form_ = $_footer.prev('div.panel-body').find('form')
        if ($(this)._form.isValid($_form_)) {
// console.log($_form_)
//console.log(this)
            var ev = $(this).InputLoadRequest(), request_fnt = function (_data_, textStatus, jqXHR) {
                if (!_data_.error) {
                    var $_panel_ = $_footer.parents('div[tabmodal]');
                    if ($($_panel_).hasAttr('tabmodal')) {
                        $(this).attr('data-dismiss', $($_panel_).attr('tabmodal'))
                        $(this).myModal('hide')
                        if ($($_form_).hasAttr('_form_request')) {
                            $(this)._form.setForm($('form#' + $($_form_).attr('_form_request')), _data_.success)
                        }
//console.log($_form_.attr('_form_request'))
                    }
                } else {
                    $($_form_)._formError().serveur(_data_.error)
                }
            }
            if ($_form_.attr('method') === 'post' || $_form_.attr('method') === 'POST') {
                $_form_.monPOST(request_fnt,
                        function () {
                            ev.remove()
                        }
                );
            } else if ($_form_.attr('method') === 'update' || $_form_.attr('method') === 'UPDATE') {
                $_form_.monUPDATE(request_fnt,
                        function () {
                            ev.remove()
                        }
                );
            }

        }
    })

    $('.panel-footer.submit').show(function () {
        var $_0 = $('<a/>').attr({
            'href': '#'
        }).appendTo($(this))

        var $_1 = $('<span/>').attr({
            'id': '#_1',
            'class': 'pull-left'
        }).html("Valider les informations").appendTo($_0);
        var $_2 = $('<span/>').attr({
            'id': '#_2',
            'class': 'pull-right'
        })

        var $_3 = $('<i/>').attr({
            'id': '#_3',
            'class': 'fa fa-arrow-circle-right submit'
        })

        $_3.appendTo($_2)
        $_2.appendTo($_0)

        var $_4 = $('<div/>').attr({
            'id': '#_4',
            'class': 'clearfix'
        }).appendTo($_0)
        var $_form_, $_footer = $(this)
        $_0.click(function (e) {
            e.preventDefault()
            $_form_ = $_footer.prev('div.panel-body').find('form')
            if ($(this)._form.isValid($_form_)) {
                //console.log($_form_)
                var ev = $_footer.InputLoadRequest(), request_fnt = function (_data_, textStatus, jqXHR) {
                    if (!_data_.error) {
                        var $_panel_ = $_footer.parents('div[tabmodal]');
                        if ($($_panel_).hasAttr('tabmodal')) {
                            $(this).attr('data-dismiss', $($_panel_).attr('tabmodal'))
                            $(this).myModal('hide')
                            if ($($_form_).hasAttr('_form_request')) {
                                $(this)._form.setForm($('form#' + $($_form_).attr('_form_request')), _data_.success)
                            }
                            //console.log($_form_.attr('_form_request'))
                        }
                    } else {
                        $($_form_)._formError().serveur(_data_.error)
                    }
                }
                if ($_form_.attr('method') === 'post' || $_form_.attr('method') === 'POST') {
                    $_form_.monPOST(request_fnt,
                            function () {
                                ev.remove()
                            }
                    );
                } else if ($_form_.attr('method') === 'update' || $_form_.attr('method') === 'UPDATE') {
                    $_form_.monUPDATE(request_fnt,
                            function () {
                                ev.remove()
                            }
                    );
                }

            }
        })
        /*
         $_is = $(this).next("a[class*='input-btn-new']")
         if (!$_is.hasClass('input-btn-new')) {
         $(this).after('<a class="btn btn-block-inline btn-social btn-dropbox input-btn-new"><i class="fa fa-plus-circle"></i></a>')
         }
         $btn = $(this).next("a[class*='input-btn-new']")
         $btn.attr('data-modal',$(this).attr('box'))
         */
    })


    /*$('form').keyup(function (e) {
     $required = true;
     $(this).find('input[required]').each(function () {
     if ($(this).val().trim() == '') {
     $required = false;
     }
     $(this).after($('<i/>').attr({
     'id': '#_00',
     'class': 'fa fa-warning'
     }).css({
     'position': 'absolute',
     'top': '9px',
     'right': '16px',
     'color': 'red'
     }))
     //console.log($(this)._form())
     })
     
     if (!$required)
     return false;
     $footer = $(this).parents('div.panel-body').next('panel-footer.submit')
     
     })*/

})



/*
 * 
 
 <span class="pull-left">Valider les informations</span><span class="pull-right"><i class="fa fa-arrow-circle-right fa-warning "></i></span><div class="clearfix"></div>
 $donne = $(this).serializeArray()
 console.log($donne)
 */