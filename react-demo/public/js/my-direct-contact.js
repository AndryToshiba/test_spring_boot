/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function ($) {

    $.fn.MyDirectChat = function (options) {

        $('<style> .input-group-addon, .input-group-btn {white-space: nowrap;vertical-align: middle;}' +
                '.my-direct-chat-contacts-open .my-direct-chat-contacts {-webkit-transform: translate(0, 0);-ms-transform: translate(0, 0);-o-transform: translate(0, 0);transform: translate(0, 0);}' +
                '.my-direct-chat .panel-body {border-bottom-right-radius: 0;border-bottom-left-radius: 0;position: relative;overflow-x: hidden; padding: 0;}' +
                '.my-direct-chat-messages, .my-direct-chat-contacts {-webkit-transition: -webkit-transform .5s ease-in-out;-moz-transition: -moz-transform .5s ease-in-out;-o-transition: -o-transform .5s ease-in-out;transition: transform .5s ease-in-out;}' +
                '.my-direct-chat-messages {-webkit-transform: translate(0, 0);-ms-transform: translate(0, 0);-o-transform: translate(0, 0);transform: translate(0, 0);padding: 10px;height: 250px;overflow: auto;}' +
                '.my-direct-chat-msg:after {clear: both;}' +
                '.my-direct-chat-msg:before{content: " ";display: table;}' +
                '.my-direct-chat-msg{ margin-bottom: 10px;}' +
                '.my-direct-chat-msg, my-direct-chat-text {display: block;}' +
                '.my-direct-chat-info {display: block;margin-bottom: 2px;font-size: 12px;}' +
                '.my-direct-chat-img {border-radius: 50%;float: left;width: 40px;height: 40px;}' +
                '.right .my-direct-chat-img {float: right;}' +
                '.my-direct-chat-success .right>.my-direct-chat-text { background: #d2d6de; border-color: #d2d6de;color: #444;}' +
                '.right .my-direct-chat-text {margin-right: 50px;margin-left: 0;}' +
                '.my-direct-chat-success .right>.my-direct-chat-text:after, .my-direct-chat-success .right>.my-direct-chat-text:before {border-left-color: #d2d6de;}' +
                '.right .my-direct-chat-text:after, .right .my-direct-chat-text:before {right: auto;left: 100%;border-right-color: transparent;border-left-color: #00a65a;}' +
                '.my-direct-chat-text {border-radius: 5px;position: relative;padding: 5px 10px;background: #00a65a;border: 1px solid #00a65a;margin: 5px 0 0 50px; color: #fff;}' +
                '.my-direct-chat-msg, my-direct-chat-text {display: block;}' +
                '.my-direct-chat-text:before {border-width: 6px;margin-top: -6px;}' +
                '.my-direct-chat-text:after, my-direct-chat-text:before {position: absolute;right: 100%;top: 15px;border: solid transparent;border-right-color: #d2d6de;content: " ";height: 0;width: 0; pointer-events: none;}' +
                '.my-direct-chat-text:after { border-width: 5px;margin-top: -5px;}' +
                '.my-direct-chat-contacts {-webkit-transform: translate(101%, 0);-ms-transform: translate(101%, 0);-o-transform: translate(101%, 0);transform: translate(101%, 0);position: absolute;top: 0;bottom: 0;height: 250px;width: 100%;background: #222d32;color: #fff;overflow: auto;}' +
                '.contacts-list{list-style: none;margin: 0;padding: 0;}' +
                '.contacts-list>li:last-of-type {border-bottom: none;}' +
                '.contacts-list>li {border-bottom: 1px solid rgba(0,0,0,0.2);padding: 10px;margin: 0;}' +
                '.contacts-list>li:before, .contacts-list>li:after {content: " ";display: table;}' +
                '.contacts-list>li:after {clear: both;}' +
                '.contacts-list-img {border-radius: 50%;width: 40px;float: left;}' +
                '.contacts-list-info {margin-left: 45px;color: #fff;}' +
                '.contacts-list-name {font-weight: 600;}' +
                '.contacts-list-name, .contacts-list-status {display: block;}' +
                '.contacts-list-msg {color: #999;}</style>').appendTo($('head'));
        var element = $(this), newMsg = function (self, option) {
            $(this).each(function () {
                self.html('');
                for (var chatMsg in option) {

                    if (parseInt(option[chatMsg].status) === 0) {
                        self.append($('<div/>', {'class': 'my-direct-chat-msg'}).append(
                                $('<div/>', {'class': 'my-direct-chat-info clearfix'}).append(
                                $('<span/>', {'class': 'my-direct-chat-name pull-left'}).html(option[chatMsg].envoi).clone(true)
                                ).append(
                                $('<span/>', {'class': 'my-direct-chat-timestamp pull-right'}).html(option[chatMsg].date).clone(true)
                                ).clone(true)
                                ).append(
                                $('<img/>', {'class': 'my-direct-chat-img', 'src': option[chatMsg].photo_envoi, 'alt': 'Message User Image'}).clone(true)
                                ).append(
                                $('<div/>', {'class': 'my-direct-chat-text'}).html(option[chatMsg].msg).clone(true)
                                ).clone(true)
                                );
                    } else if (parseInt(option[chatMsg].status) === 1) {
                        self.append($('<div/>', {'class': 'my-direct-chat-msg right'}).append(
                                $('<div/>', {'class': 'my-direct-chat-info clearfix'}).append(
                                $('<span/>', {'class': 'my-direct-chat-name pull-right'}).html(option[chatMsg].recoi).clone(true)
                                ).append(
                                $('<span/>', {'class': 'my-direct-chat-timestamp pull-left'}).html(option[chatMsg].date).clone(true)
                                ).clone(true)
                                ).append(
                                $('<img/>', {'class': 'my-direct-chat-img', 'src': option[chatMsg].photo_recoi, 'alt': 'Message User Image'}).clone(true)
                                ).append(
                                $('<div/>', {'class': 'my-direct-chat-text'}).html(option[chatMsg].msg).clone(true)
                                ).clone(true)
                                );
                    }
                }
            });
            return self;
        }, newContact = function (self, option, msg, callback) {
            $(this).each(function () {
                self.html('');
                for (var item in option) {
                    self.append($('<li/>').append(
                            $('<div/>', {'href': '#'}).append(
                            $('<img/>', {'class': 'contacts-list-img', 'src': option[item].photo, 'alt': 'User Image'}).clone(true)
                            ).append(
                            $('<div/>', {'class': 'contacts-list-info', 'style': 'cursor: pointer;'}).append(
                            $('<span/>', {'class': 'contacts-list-name'}).html(option[item].username).append(
                            $('<small/>', {'class': 'contacts-list-date pull-right'}).html(option[item].date).clone(true)
                            ).clone(true)
                            ).append(
                            $('<span/>', {'class': 'contacts-list-msg'}).html(option[item].msg).clone(true)
                            ).click(function () {
                        if (typeof callback === 'function') {
                            callback.apply(this, [msg, option[item]]);
                            self.parents('div[my-direct-chat]').each(function () {
                                $(this).toggleClass('my-direct-chat-contacts-open');
                            })
                        }
                    }).clone(true)
                            ).clone(true)
                            ).clone(true)
                            );
                }
            });
            return self;
        }

        element.each(function () {
            var el = $(this);
            el.find('[data-widget="my-direct-chat"]').each(function () {
                $(this).click(function (e) {
                    el.toggleClass('my-direct-chat-contacts-open');
                    var my_direct_chat = $(this).parents('div.my-direct-chat'),
                            framMSG = el.find('div.my-direct-chat-messages'),
                            framContact = el.find('div.my-direct-chat-contacts .contacts-list');
                    if (el.hasClass('my-direct-chat-contacts-open')) {

                        $(this).customerPOST(function (data, textStatus, jqXHR) {
                            if (!data.error) {
                                var datacontact = [];
                                for (var val in data.success) {
                                    datacontact[val] = {
                                        'photo': data.success[val].photo,
                                        'username': data.success[val].username,
                                        'date': data.success[val].created_at,
                                        'msg': data.success[val].MSG_SIGNALE,
                                        'send': data.success[val].send
                                    };
                                }
                                newContact(framContact, datacontact, framMSG, function (msg, data_) {
                                    /*** get msg ***/
                                    $(this).customerPOST(function (_data_, textStatus, jqXHR) {
                                        if (_data_.success) {
                                            var datamsg = [];
                                            for (var val in _data_.success) {
                                                datamsg[val] = {
                                                    'photo_recoi': _data_.success[val].photo_recoi,
                                                    'photo_envoi': _data_.success[val].photo_envoi,
                                                    'recoi': _data_.success[val].recoi,
                                                    'envoi': _data_.success[val].envoi,
                                                    'date': _data_.success[val].created_at,
                                                    'msg': _data_.success[val].MSG_SIGNALE,
                                                    'status': _data_.success[val].STATUS_SIGNALE
                                                };
                                            }
                                            $('form#my-direct-chat-form').find('[name="send"]').each(function () {
                                                $(this).val(data_.send)
                                            });
                                            newMsg(msg, datamsg);
                                        }
                                    }
                                    , {'IDENTIFIANT': data_.send}, null, options.url_msg);
                                    /*** get msg ***/
                                });
                            }
                        }
                        , null, null, options.url_contact)
                    }
                })
            })

            el.find('form#my-direct-chat-form').dataFnct(function (e, res, set) {
                console.log(set)
                $(this).customerPOST(function (_data_, textStatus, jqXHR) {
                    if (_data_.success) {
                        var datamsg = [];
                        for (var val in _data_.success) {
                            datamsg[val] = {
                                'photo_recoi': _data_.success[val].photo_recoi,
                                'photo_envoi': _data_.success[val].photo_envoi,
                                'recoi': _data_.success[val].recoi,
                                'envoi': _data_.success[val].envoi,
                                'date': _data_.success[val].created_at,
                                'msg': _data_.success[val].MSG_SIGNALE,
                                'status': _data_.success[val].STATUS_SIGNALE
                            };
                        }
                        el.find('form#my-direct-chat-form').find('[name="send"]').each(function () {
                            $(this).val(set.send)
                        });
                        newMsg(el.find('div.my-direct-chat-messages'), datamsg);
                    }
                }
                , {'IDENTIFIANT': set.send}, null, options.url_msg);
            }, 'ajouter');
        })
    }
}
)

