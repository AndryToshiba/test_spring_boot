/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var b = $('body').width();
var h = $('body').height();
var el = $('body').children().length;
var w_nav = window.innerWidth;
var h_nav = window.innerHeight;
var scr_h = screen.height;
var scr_w = screen.width;
var base_url = 'http://' + window.location.host + '/';
var DisplayInfo, TableListe, is_action = {modifier: 1, supprimer: 2, detail: 3};
var callback_ajax = [], bibliotheque = {}//funct _ 

$(document).ajaxComplete(function (event, xhr, options) {
    var k = 0;
    while (callback_ajax[k]) {
        if (typeof callback_ajax[k] == 'function')
            callback_ajax[k].call()
        else if (typeof callback_ajax[k] == 'object') {
            for (var h in callback_ajax[k])
                if (typeof callback_ajax[k][h] === 'function')
                    callback_ajax[k][h].call()
        }//fnct

    }
    k = 0
    while (options && xhr.status === 200 && options.callback && options.callback[k]) {
        options.callback[k].call(this, event, xhr.responseJSON)
        k++
    }
});

function addAjaxCallback(fnct) {
    if (typeof fnct === 'function')
        callback_ajax.push(fnct)
}

var generateChars = function (length) {
    var chars = '';
    for (var i = 0; i < length; i++) {
        var randomChar = Math.floor(Math.random() * 36);
        chars += randomChar.toString(36);
    }

    return chars;
};

var bouttonAction = function ($propiete) {
    var params = $.extend(true, {}, {
        class: 'fa-edit',
        title: 'action',
        action: null,
        session: null,
        callback: null
    }, $propiete = $propiete || {}), element = $(this)
    return $('<div/>',
            {
                'class': 'pull-right box-tools btn-remove',
                'style': 'margin-top:0px;margin-bottom:0px;'
            }
    ).append(
            $('<button/>',
                    {
                        'type': 'button',
                        'class': 'btn btn-info btn-sm',
                        'style': 'font-size: 8px;',
                        'title': 'action'
                    }
            ).append(
            $('<i/>',
                    {
                        'class': 'fa fa-edit'
                    }
            ).clone(true)
            ).click(
            function () {
                if (typeof params.callback === 'function')
                    params.callback.apply($(this), [$(this), params.action, params.session])
            }).clone(true)
            )

    /*
     
     <div class="pull-right box-tools btn-remove" style="margin-top:0px;margin-bottom:0px;">
     <button type="button" class="btn btn-info btn-sm" style="font-size: 8px;" title="modifier"><i class="fa fa-edit"></i></button></div>
     
     */
}

function Bibliotheque() {
    this.bibliotheque_key_ = bibliotheque || {};
    this.add = function (...biblio) {

        if (typeof biblio === 'object') {
            var k = 0
            while (biblio[k]) {
                for (var key in biblio[k]) {
                    if (!(key in this.bibliotheque_key_) && typeof biblio[k][key] === 'string')
                        this.bibliotheque_key_[key] = biblio[k][key];
                }
                k++
            }
        }
        bibliotheque = this.bibliotheque_key_;
        return this.bibliotheque_key_;
    }

    this.edit = function (biblio) {
        if (typeof biblio === 'object' && typeof biblio.value === 'string') {
            for (var key in biblio)
                this.bibliotheque_key_[key] = biblio[key];
        }
        bibliotheque = this.bibliotheque_key_;
        return this.bibliotheque_key_;
    }

    this.is = function (chercher) {
        if (typeof chercher === 'object') {
            var k = 0;
            for (var l in chercher) {
                var swap = chercher
                if (typeof swap[l] === 'object' && swap[l]) {
                    chercher[l] = this.is(swap[l]);
                } else if (typeof swap[l] === 'string' || swap[l] === null) {
                    if (l in this.bibliotheque_key_)
                        chercher[l] = {biblio: this.bibliotheque_key_[l], value: swap[l]};
                    else
                        chercher[l] = {biblio: l, value: swap[l]};
                }
            }
        }
        return chercher;

    }

    return this;
}

$(document).ready(function ($) {

    var url = window.location;
    var element = $('ul.nav a').filter(function () {
        return this.href == url || url.href.indexOf(this.href) == 0;
    }).addClass('active').parent().parent().addClass('in').parent();
    if (element.is('li')) {
        element.addClass('active');
    }

    $.fn.dataFnct = function (_fnct) {
        var _this = $(this), fnct = [], gn = 0

        if (typeof _fnct === 'function') {
            if ($(this).data() && $(this).data().fnct) {
                while ($(this).data().fnct[gn]) {
                    if (typeof $(this).data().fnct[gn] === 'function')
                        fnct.push($(this).data().fnct[gn])
                    gn++
                }
            }
            fnct.push(_fnct)
            _this.data('fnct', fnct)
        }
        return _this
    }

    $.fn.hasAttr = function (attribute) {
//console.log($(this))
        if (typeof $(this)[0] === 'object') {
//console.log($(this).attr(attribute))
            return (typeof $(this).attr(attribute) === 'string')
        }
        return false
    }
    $.fn.outerCss = function (returns) {
        if (typeof returns === 'string') {
            if (returns === 'this')
                return {
                    left: parseInt($(this).css('padding-left')) + parseInt($(this).css('margin-left')),
                    right: parseInt($(this).css('padding-right')) + parseInt($(this).css('margin-right')),
                    top: parseInt($(this).css('padding-top')) + parseInt($(this).css('margin-top')),
                    bottom: parseInt($(this).css('padding-bottom')) + parseInt($(this).css('margin-bottom'))
                }
            if (returns === 'parent')
                return {
                    left: parseInt($(this).parent().css('padding-left')),
                    right: parseInt($(this).parent().css('padding-right')),
                    top: parseInt($(this).parent().css('padding-top')),
                    bottom: parseInt($(this).parent().css('padding-bottom'))
                }
        } else
            return {
                left: parseInt($(this).parent().css('padding-left')) + parseInt($(this).css('padding-left')) + parseInt($(this).css('margin-left')),
                right: parseInt($(this).parent().css('padding-right')) + parseInt($(this).css('padding-right')) + parseInt($(this).css('margin-right')),
                top: parseInt($(this).parent().css('padding-top')) + parseInt($(this).css('padding-top')) + parseInt($(this).css('margin-top')),
                bottom: parseInt($(this).parent().css('padding-bottom')) + parseInt($(this).css('padding-bottom')) + parseInt($(this).css('margin-bottom'))
            }
    }

    $.fn.generateChars = function (length) {
        var chars = '';
        for (var i = 0; i < length; i++) {
            var randomChar = Math.floor(Math.random() * 36);
            chars += randomChar.toString(36);
        }

        return chars;
    };
    $.fn.InputLoadRequest = function () {
        var _l = 'load-' + $(this).generateChars(4), self = $(this)

        $('<i/>').attr({
            'class': 'fa fa-spinner',
            //'loading': _l
        }).css({
            'position': 'relative', //'position': 'absolute',
            //'top': '69%',
            //'left': '50%',
            //'margin-left': '-15px',
            'margin-top': (((parseInt($(this).outerHeight()) - 22) / 2) < 0 ? 0 : ((parseInt($(this).outerHeight()) - 22) / 2)) + 'px', //'margin-top': '-15px',
            'color': 'rgb(36, 216, 83)',
            'font-size': '22px',
            '-webkit-animation': 'fa-spin 2s infinite linear',
            'animation': 'fa-spin 2s infinite linear'
        }
        ).appendTo($('<div/>').attr({
            'loading': _l
        }).css({
            'position': 'absolute',
            'text-align': 'center',
            'margin-top': (-1 * parseInt(self.outerHeight())) + 'px',
            //'top': parseInt($(this).offset().top),
            /*'left': a = function () {var _i_ = parseInt(self.offset().left) - (parseInt(self.width()) + self.outerCss('this').left);alert(_i_ +' :i');if (_i_ < 0 || self.css('position') === 'relative') {alert(self.outerCss('parent').left);return 0 + self.outerCss('parent').left;} elsereturn _i_ + self.outerCss('parent').left;},*/
            'left': a = function () {
                var _i_ = parseInt(self.parent().width()) - parseInt(self.outerWidth())
                if (self.css('position') === 'relative')
                    return self.outerCss().left
                else if (_i_ < 0) {
                    return 'unset'//0
                } else
                    return _i_ + self.outerCss('parent').left
            },
            'width': self.outerWidth(),
            'height': self.outerHeight(),
            'background-color': '#868b90bd'
        }).appendTo(self.parent()))

        return {
            self: $('div[loading="' + _l + '"]'),
            remove: function () {
                $('div[loading="' + _l + '"]').remove()
            }
        }
    }

    try {
        $('#app').css({
            'height': h_nav,
        })
    } catch (exception) {

    }
    try {
        $('.toggle-btn').click(function () {
            $('.' + $(this).attr('data-toggle')).show();
        });
    } catch (exception) {

    }
    try {
        $('.auto-hide').mouseleave(function () {
            $(this).hide();
        });
    } catch (exception) {

    }


    $('main').css({
//'background-color':'red',
        'height': 97.6 - (($('nav').height() * 100) / window.innerHeight) + '%'
    })
    try {
        $('.bg-image').show(function () {
            $(this).css({
                'background-size': $(this).width() + 42 + 'px ' + ($(this).height() + (scr_h / $(this).height() * 100)) + 'px'
            });
        });
    } catch (exception) {

    }


    /*$.fn.parentOperation = function ($this) {
     var b_$this = $this.width();
     var h_$this = $this.height();
     var el_$this = $this.children().length;
     return {a: b_$this, b: h_$this, c: el_$this};
     }*/

    try {
        $this = $('.vertical-align');
        $parent = $this.parent();
        $_s = $parent.height() - $this.height();
        if ($_s) {
            $_val_wdth = $_s / 2;
            $this.css({
                'margin-top': $_val_wdth,
                'margin-bottom': $_val_wdth,
                //"background-color": "#98bf21"
            });
        }

    } catch (exception) {
        alert(exception);
    }

    try {
        $('.info-cache').show(function () {
            $(this).hide();
        });
    } catch (exception) {

    }
    try {
        $this = $('.btn-remove');
        $parent = $this.parents('.box-removable');
    } catch (exception) {

    }

    try {
        $('.info-cache').mouseleave(function () {
            $(this).hide('slow');
        });
    } catch (exception) {

    }


    $('.panel-table-list-content').css({
//'width': $('.panel-table-list').width()
        'width': parseInt(window.innerWidth) - 50
    })

    $('[data-toggle="collapse"]').on('rendreWidth', function (event) {
        $('.panel-table-list-content').css({
//'width': $('.panel-table-list').width()
            'width': parseInt(window.innerWidth) - 50
        })
    })


    /*
     $('.form-group label').show(function () {
     alert($('.form-group label').innerWidth())
     switch (($('.form-group label').width() - $('.form-group label font').width()) >= 0 ? 1 : 0) {
     case 0:
     $('.form-group label').css({
     'overflow': 'hidden',
     'text-overflow': 'ellipsis'
     })
     break;
     
     case 1:
     $('.form-group label').css({
     'overflow': 'initial',
     'text-overflow': 'initial'
     });
     break;
     }
     })
     */

    $('.form-group label').show(function () {
        $("input[id='" + $(this).attr('for') + "'").attr('placeholder', $(this).html());
    })

    $("input[class*='btn-new']").show(function () {
        var $_is = $(this).next("a[class*='input-btn-new']"), title = $(this).hasAttr('btn-title') ? $(this).attr('btn-title') : 'Créer nouveau'
        if (!$_is.hasClass('input-btn-new')) {
            $(this).after('<a class="btn btn-block-inline btn-social input-btn-new" title="' + title + '"><i class="fa fa-plus-circle"></i></a>')
        }
        var $btn = $(this).next("a[class*='input-btn-new']")
        if (typeof $(this).parents('form').attr('id') === 'string')
            $btn.attr('_form_reponse', $(this).parents('form').attr('id'))
        $btn.attr('data-modal', $(this).attr('box'))
    })


    /*
     * 
     '<a class="btn btn-block-inline btn-social btn-dropbox input-btn-new"><i class="fa fa-plus-circle"></i></a>'
     * mise à jours
     * 
     */

    /*
     * 
     * function efface_formulaire () {
     $(':input')
     .not(':button, :submit, :reset, :hidden')
     .val('')
     .prop('checked', false)
     .prop('selected', false);
     }
     */
    /*$.each(['show', 'hide'], function (i, ev) {
     var el = $.fn[ev];
     $.fn[ev] = function () {
     this.trigger(ev)
     return el.apply(this, arguments)
     }
     })
     */

    $.fn.addTitle_old = function (cl_css = 'title-info') {
        $_ele_ = $(this).has('[title]')
        if ($_ele_)
            return $(this).show(function () {
                /*
                 
                 title-warning{
                 'visibility': 'hidden',
                 'width': 'max-content',
                 'max-width': '120px',
                 'top': '-5px',
                 'right': '20px',
                 'background-color': '#f8fafc',
                 'color': '#e3342f',
                 'border': '1px solid #e3342f',
                 'text-align': 'center',
                 'padding': '5px 10px 5px 10px',
                 'border-radius': '6px',
                 //Position the tooltip text - see examples below!
                 'position': 'absolute',
                 'z-index': '1',
                 'transition': 'opacity 1s'
                 }
                 
                 */

                $("<span/ > ").html($(this).attr('title')).appendTo($(this)).attr({
                    'class': 'title ' + cl_css
                })
                $(this).removeAttr('title').hover(function () {
                    $(this).children('.title').css('visibility', 'visible')
                }).mouseleave(function () {
                    //$(this).children('.title').css('visibility', 'hidden')
                })

            }
            )
    }

    $.fn.addTitle = function (cl_css = 'title-info') {
        var el = $(this)
        if ($(this).hasAttr('title-old') && !$(this).hasAttr('title_'))
            return this
        if ($(this).hasAttr('title') || $(this).hasAttr('title_'))
            $(this).off('mouseenter')
        return $(this).mouseenter(function (event) {
            var _title_ = $(this).attr('title')
            $(this).attr('title-old', _title_)
            var _sess_ = '_info_' + new Date().getTime()
            $(this).removeAttr('title')
            var x = $(this).offset()
            var _this_ = $("<span/ > ").html(_title_).appendTo('body').attr({
                'class': 'title ' + cl_css,
                'sess-actif': _sess_
            })
            _this_.css({
                'visibility': 'visible',
                'z-index': '100000',
                'right': 'unset',
                'top': parseInt(event.clientY) - (parseInt(_this_.height()) / 2) + 'px',
                'left': parseInt(event.clientX) - (parseInt(_this_.width()) + 36) + 'px'
            })

            $(this).mouseleave(function () {
                $("[sess-actif='" + _sess_ + "']").remove()
                if ($(this).hasAttr('title-old')) {
                    $(this).attr('title', $(this).attr('title-old'))
                    $(this).removeAttr('title-old')
                } else if (!$(this).hasAttr('title'))
                    $(this).off('mouseenter')
            })

        }
        )
        return el
    }

    $('[title]').addTitle()

    /*
     * 
     * 
     * var x = $("p").offset();
     alert("Top: " + x.top + " Left: " + x.left);
     
     .tooltip-right::after {
     content: "";
     position: absolute;
     top: 50%;
     right: 100%;
     margin-top: -5px;
     border-width: 5px;
     border-style: solid;
     border-color: transparent #555 transparent transparent;
     }
     
     */

    $(window).resize(function () {
// alert(window.innerHeight);
        try {
            $('#app').css({
                'height': window.innerHeight,
            })
        } catch (exception) {

        }

        try {
            $('.bg-image').on('show', function () {
                $(this).css({
                    'background-size': $(this).width() + 42 + 'px ' + ($(this).height() + (scr_h / $(this).height() * 100)) + 'px'
                });
            });
        } catch (exception) {

        }

        try {
            $this = $('.vertical-align');
            $parent = $this.parent();
            $_s = $parent.height() - $this.height();
            if ($_s) {
                $_val_wdth = $_s / 2;
                $this.css({
                    'margin-top': $_val_wdth,
                    'margin-bottom': $_val_wdth,
                    //"background-color": "#98bf21"
                });
            }

        } catch (exception) {
            alert(exception);
        }

        $('.panel-table-list-content').css({
            'width': parseInt(window.innerWidth) - 50
        })
    });
    /*$.fn._form = function () {
     return {
     isValid:this.context.validity.valid,
     error:this.context.validationMessage
     }
     }*/

    $.fn.controleInput = function () {
        var element = $(this), dd = function (val) {

            var i = val.split(' '), resulta = val
            for (var j = 0; j < i.length; j++) {
                resulta = j == 0
                        ?
                        $.trim(i[j]) !== '' ? $.trim(i[j]).substr(0, 1).toUpperCase() + $.trim(i[j]).substr(1) : ''
                        :
                        (resulta !== ''
                                ?
                                resulta += ' ' + ($.trim(i[j]) !== '' ? $.trim(i[j]).substr(0, 1).toUpperCase() + $.trim(i[j]).substr(1) : '')
                                :
                                $.trim(i[j]) !== '' ? $.trim(i[j]).substr(0, 1).toUpperCase() + $.trim(i[j]).substr(1) : '')
            }
            return $.trim(resulta)
        }

        element.each(function () {
            $(this).focusout(function () {
                var val = $(this).val()
                if ($(this).hasClass('upercase'))
                    $(this).val(val.toUpperCase())
                else
                    $(this).val(dd(val))
            })
            $(this).focusin(function () {
                $(this).select()
            })
        })
    }

    $.fn._form = {
        isValid: function (form) {
            _true_ = 1
            $(form).find('input,select,textarea').each(function () {
                if ($(this).is('input') || $(this).is('select') || $(this).is('textarea')) {
                    if ($(this).context.validity.valid) {
//console.log($(this).nextAll("i[class*='fa-warning']"))
                        if ($(this).nextAll("i[class*='fa-warning']").length)
                            $(this).nextAll("i[class*='fa-warning']").each(function () {
                                $(this).remove()
                            })
                    } else {
                        if ($(this).nextAll("i[class*='fa-warning']").length)
                            $(this).nextAll("i[class*='fa-warning']").each(function () {
                                $(this).remove()
                            })
                        $(this).after($('<i/>').attr({
                            'id': '#_00',
                            'class': 'fa fa-warning',
                            'title': $(this)[0].validationMessage
                        }).css({
                            'position': 'absolute',
                            'top': '9px',
                            'right': '16px',
                            'color': 'red',
                            'z-index': '1'
                        }).addTitle('title-warning'))
                        _true_ = 0
                    }
                }
            })

            return _true_
        },
        news: function (form) {
            $(form).find('input, select, textarea').each(function () {
                //console.log($(this).is('textarea'))
                if ($(this).is(':text') || $(this).is('[type="date"]') || $(this).is('[type="email"]') || $(this).is('select') || $(this).is('textarea')) {
                    $(this).val('')//.attr('title', $(this)[0].validationMessage).addTitle()
                }
                if ($(this).nextAll("i[class*='fa-warning']").length)
                    $(this).nextAll("i[class*='fa-warning']").each(function () {
                        $(this).remove()
                    })

            })

            $(form).find('span[data-id-mySelected]').each(function () {
                if ($(this).is('span')) {
                    $(this).attr('title', 'Veuillez selectioner une valeur').html('Sélectionnez ').addTitle()
                }
                if ($(this).nextAll("i[class*='fa-warning']").length)
                    $(this).nextAll("i[class*='fa-warning']").each(function () {
                        $(this).remove()
                    })

            })
            $('form').trigger('new', [form])
            return this
        },
        setForm: function (form, data) {
            if (typeof data === 'object' && $(form).is('form')) {
                for (x in data) {
                    var $_input_ = $(form).find('input[name="' + x + '"],select[name="' + x + '"],textarea[name="' + x + '"]')
                    console.log($($_input_).is('input[name="' + x + '"]'))
                    if ($($_input_).is('input[name="' + x + '"]') || $($_input_).is('select[name="' + x + '"]') || $($_input_).is('textarea[name="' + x + '"]')) {
                        $_input_.val(data[x])
                        if ($_input_.hasAttr('data-selected'))
                        {
                            $('[data-id-myselected="' + $_input_.attr('data-selected') + '"]').html($_input_.val())
                        }
                    }
                }
                if (form.hasAttr('data-update')) {
                    $('<input/>', {name: 'UPDATE', hidden: 'hidden', value: data[form.attr('data-update')]}).appendTo(form)
                }
            }
        }

    }

    $.fn._formError = function () {
        $_this = $(this)
        return {
            serveur: function (data) {
                if (data) {
                    console.log(data)
                    console.log(data.responseJSON.errors)
                    console.log(typeof data.responseJSON.errors)
                    if (typeof data.responseJSON.errors === 'object')
                        console.log('object')
                    for (x in data.responseJSON.errors) {
                        console.log(typeof data.responseJSON.errors[x])
                        if (typeof data.responseJSON.errors[x] === 'object') {
                            console.log('object')
                            $_input_ = $($_this).find('input[name="' + x + '"], select[name="' + x + '"], textarea[name="' + x + '"]')
                            if ($($_input_).hasAttr('name') && typeof data.responseJSON.errors[x][0] === 'string') {
                                console.log('string')
                                if ($($_input_).nextAll("i[class*='fa-warning']").length)
                                    $($_input_).nextAll("i[class*='fa-warning']").each(function () {
                                        $(this).remove()
                                    })
                                $($_input_).after($('<i/>').attr({
                                    'id': '#_00',
                                    'class': 'fa fa-warning',
                                    'title': data.responseJSON.errors[x][0]
                                }).css({
                                    'position': 'absolute',
                                    'top': '9px',
                                    'right': '16px',
                                    'color': 'red',
                                    'z-index': '1'
                                }).addTitle('title-warning'))
                            }
                        }

                    }


                }
            }
        }
    }

    $.fn.removeInputErrer = function () {
        if ($(this)[0].validity.valid)
            if ($(this).nextAll("i[class*='fa-warning']").length)
                $(this).nextAll("i[class*='fa-warning']").remove()
        return $(this)
    }

    $('input,select').keyup(function (e) {
        $(this).removeInputErrer()
    })

    $('input[type="date"], select,input').change(function (e) {
        $(this).removeInputErrer()
    })


    /*$.fn._form = {
     isValid: function () {
     return $(this)
     },
     isFormValid: function () {
     return $(this)//context.validity.valid
     }
     
     }*/

    $.fn.monPOST = function (methode = null, even, action) {
        var _this_ = $(this)
        if (!_this_.is('form') && typeof action === 'string')
            _this_ = $('<form/>', {
                'action': action
            }).append($('<input/>', {
                'name': 'form'
            }).clone())
        $.ajax({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token-x"]').attr('content')
            },
            url: base_url + _this_.attr('action'),
            data: _this_.serialize(),
            callback: _this_.data('fnct'),
            type: 'POST',
            success: function (_data_, textStatus, jqXHR) {
                if (methode) {
                    var _data__ = {success: _data_, error: null}
                    methode(_data__, textStatus, jqXHR);
                }

            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (methode) {
                    var _data_ = {success: null, error: jqXHR}
                    methode(_data_, textStatus, jqXHR);
                }
            },
            complete: function (jqXHR, textStatus) {
                if (even && typeof even === 'function')
                    even()
            }
        });
    }

    /*
     * 
     * @param {type} methode
     * @param {type} data
     * @param {type} even
     * @param {type} action
     * @returns {globaleL#101.$.fn}
     */
    $.fn.customerPOST = function (methode = null, data = null, even, action) {
        var _this_ = $(this)
        $.ajax({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token-x"]').attr('content')
            },
            url: action.search("://") > 0 ? action : (base_url + action),
            data: data,
            callback: _this_.data('fnct'),
            type: 'POST',
            success: function (_data_, textStatus, jqXHR) {
                if (methode) {
                    var _data__ = {success: _data_, error: null}
                    methode(_data__, textStatus, jqXHR);
                }

            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (methode) {
                    var _data_ = {success: null, error: jqXHR}
                    methode(_data_, textStatus, jqXHR);
                }
            },
            complete: function (jqXHR, textStatus) {
                if (even && typeof even === 'function')
                    even()
            }
        });
        return this
    }

    $.fn.monGET = function (action, data = null, methode = null) {
        var _this_ = $(this)
        $.ajax({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: base_url + action,
            data: data,
            type: 'GET',
            success: function (_data_, textStatus, jqXHR) {
                if (methode) {
                    _data_ = {success: _data_, error: null}
                    methode(_data_, textStatus, jqXHR);
                }

            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (methode) {
                    var _data_ = {success: null, error: jqXHR}
                    methode(_data_, textStatus, jqXHR);
                }
            }
        });
    }

    /*$.fn.monGET = function (action, data = null, methode = null) {
     var _this_ = $(this)
     $.ajax({
     headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
     },
     url: base_url + _this_.attr('action'),
     data: _this_.serialize(),
     type: 'GET',
     success: function (_data_, textStatus, jqXHR) {
     if (methode) {
     _data_ = {success: _data_, error: null}
     methode(_data_, textStatus, jqXHR);
     }
     
     },
     error: function (jqXHR, textStatus, errorThrown) {
     if (methode) {
     _data_ = {success: null, error: jqXHR}
     methode(_data_, textStatus, jqXHR);
     }
     }
     });
     }*/

    $.fn.monDELETE = function (action, data, methode = null, even) {
        $_ = {success: null, error: null}
        $.ajax({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: base_url + action,
            data: data,
            type: 'DELETE',
            success: function (_data_, textStatus, jqXHR) {
                if (methode) {
                    _data_ = {success: _data_, error: null}
                    methode(_data_, textStatus, jqXHR);
                }

            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (methode) {
                    var _data_ = {success: null, error: jqXHR}
                    methode(_data_, textStatus, jqXHR);
                }
            },
            complete: function (jqXHR, textStatus) {
                if (even && typeof even === 'function')
                    even()
            }
        });
    }

    $.fn.monUPDATE = function (methode = null, even, action) {
        var _this_ = $(this)
        $.ajax({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token-x"]').attr('content')
            },
            url: base_url + _this_.attr('action') + '/update',
            data: _this_.serialize(),
            callback: _this_.data('fnct'),
            type: 'PUT',
            success: function (_data_, textStatus, jqXHR) {
                if (methode) {
                    var _data__ = {success: _data_, error: null}
                    methode(_data__, textStatus, jqXHR);
                }

            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (methode) {
                    var _data_ = {success: null, error: jqXHR}
                    methode(_data_, textStatus, jqXHR);
                }
            },
            complete: function (jqXHR, textStatus) {
                if (even && typeof even === 'function')
                    even()
            }
        });
    }

    $.fn.monUploadImage = function (methode = null, evenStrat, evenStop) {
        $(this).each(function () {
            $(this).change(function () {
                var event = {}
                if (evenStrat && typeof evenStrat === 'function')
                    evenStrat(event)
                var element = $(this), el_ = this, form = new FormData();
                form.append('file', el_.files[0]);
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token-x"]').attr('content')
                    },
                    url: base_url + element.attr('action'),
                    data: form,
                    cache: true,
                    contentType: false,
                    processData: false,
                    callback: element.data('fnct'),
                    type: 'POST',
                    success: function (_data_, textStatus, jqXHR) {
                        if (methode) {
                            var _data__ = {success: _data_, error: null}
                            methode(_data__, textStatus, jqXHR);
                        }

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        if (methode) {
                            var _data_ = {success: null, error: jqXHR}
                            methode(_data_, textStatus, jqXHR);
                        }
                    },
                    complete: function (jqXHR, textStatus) {
                        if (evenStop && typeof evenStop === 'function')
                            evenStop(event)
                    }
                });
            })
        })
    }

    $.fn.myModalRequest = function (methode = null) { //data-request
        var _this_ = $(this)
        //console.log(_this_.attr('data-request'))
        var _data_ = {success: null, error: null}
        $.ajax({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: base_url + _this_.attr('data-action'),
            data: {'modifier': _this_.attr('data-request')},
            type: 'GET',
            success: function (_data_, textStatus, jqXHR) {
                if (methode) {
                    _data_ = {success: _data_, error: null}
                    methode(_data_, textStatus, jqXHR);
                }


            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (methode) {
                    _data_ = {success: null, error: jqXHR}
                    methode(_data_, textStatus, jqXHR);
                }
            }
        });
    }

    $.fn.confirmeForm = function (title = 'Title', attr, action) {
        var _i_ = ')]/' + $(this).generateChars(4), element = $(this), fn = function (t, at, act, form) {
            this.boucle = function () {
                form.remove();
                $(this).confirmeForm(t, at, act);
            }
        }
        var modal = $('<div/>', {
            'class': 'row justify-content-center MyModal'
        }).append(
                $('<div/>', {
                    'class': 'panel panel-default',
                    'tabmodal': _i_
                }).clone(true)),
                header = $('<div/>', {
                    'class': 'panel-heading fa-warning'
                }).html(title).append(
                $('<div/>', {
                    'class': 'pull-right box-tools btn-remove'
                }).append(
                $('<button/>', {
                    'type': 'button',
                    'class': 'btn btn-danger btn-sm',
                    'data-dismiss': _i_
                }).html('<i class="fa fa-times"></i>').click(function () {
            if (typeof action === 'function')
                action.apply(this, [null, modal, new fn(title, attr, action, modal)])
        }).clone(true)
                ).clone(true)),
                body = $('<div/>', {
                    'class': 'panel-body'
                }).append(
                $('<form/>', {
                    'id': _i_
                }).html('<div class="table-responsive"></div>').clone(true)
                ),
                footer = $('<div/>', {
                    'class': 'panel-footer text-right'
                }).click(function () {
            var _y = body.find('form')
            if (_y.length && $(this)._form.isValid(_y)) {
                var i = 0, arr = _y.serializeArray(), obj = {}
                while (arr[i]) {
                    obj[arr[i].name] = arr[i].value
                    i++
                }
                action.apply(this, [obj, modal, new fn(title, attr, action, modal)])
            }
        }).html('<button type="button" class="btn btn-primary">Valider</button>')

        header.appendTo(modal.children())
        body.appendTo(modal.children())
        footer.appendTo(modal.children())
        //console.log(modal)
        modal.appendTo('body')

        var table = $('<table/>', {
            'class': 'table table-striped table-bordered table-hover'
        })
        if (typeof attr === 'object') {
            var l = 0
            while (attr[l]) {
                if (typeof attr[l] === 'object') {
                    var atr = {}
                    if (typeof attr[l].type === 'string')
                        atr['type'] = attr[l].type
                    if (typeof attr[l].pattern === 'string')
                        atr['pattern'] = attr[l].pattern
                    if (typeof attr[l].required === 'string')
                        atr['required'] = attr[l].required
                    if (typeof attr[l].minlength === 'string')
                        atr['minlength'] = attr[l].minlength
                    if (typeof attr[l].maxlength === 'string')
                        atr['maxlength'] = attr[l].maxlength
                    atr['class'] = 'form-control'
                    atr['name'] = attr[l].name.replace(/ /g, "_")
                    table.append(
                            $('<tr/>').append(
                            $('<td/>').append(
                            $('<div/>', {
                                'class': 'form-group'
                            }).append(
                            $('<label/>', {
                                'class': 'col-sm-3 control-label'
                            }).html(attr[l].label).clone(true)
                            ).append(
                            $('<div/>', {
                                'class': 'col-sm-9'
                            }).append(
                            $('<input/>', atr).keyup(function () {
                        $(this).removeInputErrer()
                    }).clone(true)
                            ).clone(true)
                            ).clone(true)
                            ).clone(true)
                            ).clone(true)
                            )
                }
                l++
            }
        }
        table.appendTo(body.find('div'))
        $('<p/>', {
            'data-modal': _i_
        }).myModal()


    }



    $.fn.optionConfirmation = function (title = 'Title', action) {
        var _i_ = ')]/' + $(this).generateChars(4), element = $(this)
        var modal = $('<div/>', {
            'class': 'row justify-content-center MyModal'
        }).append(
                $('<div/>', {
                    'class': 'panel panel-default',
                    'tabmodal': _i_
                }).clone(true)),
                header = $('<div/>', {
                    'class': 'panel-heading fa-warning'
                }).html(title).append(
                $('<div/>', {
                    'class': 'pull-right box-tools btn-remove'
                }).append(
                $('<button/>', {
                    'type': 'button',
                    'class': 'btn btn-info btn-sm',
                    'data-dismiss': _i_
                }).html('<i class="fa fa-times"></i>').click(function () {
            if (typeof action === 'function')
                action.apply(this, [modal, false])
            modal.remove()
        }).clone(true)
                ).clone(true)),
                footer = $('<div/>', {
                    'class': 'panel-footer text-right'
                }).append(
                $('<button/>', {'type': 'button', 'class': 'btn btn-info', 'style': 'float:left;'}).click(function () {
            if (typeof action === 'function')
                action.apply(this, [modal, false])
            modal.remove()
        }).html('Annuler').clone(true)
                ).append(
                $('<button/>', {'type': 'button', 'class': 'btn btn-primary', 'style': 'background-color: #e3342f80;'}).click(function () {
            if (typeof action === 'function')
                action.apply(this, [modal, true])
        }).html('Ok').clone(true)
                )

        header.appendTo(modal.children())
        footer.appendTo(modal.children())
        //console.log(modal)
        modal.appendTo('body')
        $('<p/>', {
            'data-modal': _i_
        }).myModal()


    }

    $('[data-toggle="tooltip"]').tooltip();

    DisplayInfo = function (display, _block = false) {
        var ol = $('<ol/>'), li = function () {
            return $('<li/>')
        }, dl = function () {
            return $('<dl/>', {'class': 'dl-horizontal info-liste'})
        }, block = function () {
            return $('<div/>', {'class': 'block-info-liste'})
        }
        var i = 0
        while (display && display[i] && _block) {
            if (typeof display[i].reference === 'string') {
                var head = new li(), body = new dl(), _b = new block()
                head.html(display[i].reference)
                for (var k = 0; k < display[i].content.length; k++) {
                    if (typeof display[i].content[k] === 'object') {
                        body.append($('<dt/>').html(display[i].content[k].title).clone(true)).append($('<dd/>').html('<b>:</b> ' + display[i].content[k].define).clone(true))
                    }
                }
                head.appendTo(_b)
                body.appendTo(_b)
            } else
                i = display.length + 1;
            _b.appendTo(ol)
            i++
        }

        while (display && display[i] && !_block) {
            if (typeof display[i].reference === 'string') {
                var head = new li()
                head.html(display[i].reference)
                head.appendTo(ol)
                for (var k = 0; k < display[i].content.length; k++) {
                    if (typeof display[i].content[k] === 'object') {
                        var body = new dl(), _b = new block()
                        body.append($('<dt/>').html(display[i].content[k].title).clone(true)).append($('<dd/>').html('<b>:</b> ' + display[i].content[k].define).clone(true))
                        body.appendTo(_b)
                        _b.appendTo(ol)
                    }
                }

            } else
                i = display.length + 1;
            i++
        }
        return display && i == display.length ? ol : ''
    }

    TableListe = function (self, dataTable = {data:[], colonne:[/*{id:null, value:null}*/], pagination:{link_prev:null, link_next:null}}, event = {pagPrec:null, pagSuiv:null}, btnAction = null) {
        self.css('display', 'none')
        var col = function (attr = {}) {
            return $('<td/>', attr)
        }, row = function (display = true) {
            return display ? $('<tr/>') : $('<tr/>', {'style': 'display:none;'})
        }, paginer = dataTable.pagination.link_prev || dataTable.pagination.link_next
        var i = 0, table = $('<table/>', {'class': 'table table-condensed', 'style': 'border-top: 4px solid #d2d6de !important;', 'table-id': generateChars(10)}), content = $('<tbody/>'), foot = $('<tfoot/>'), head = $('<thead/>'), _rowHead = new row(), headSet = false, notPaginate = true
        while (dataTable.data[i]) {
            var _row = new row(notPaginate), k = 0
            while (dataTable.colonne[k] && typeof dataTable.colonne[k]['id'] !== 'undefined' && typeof dataTable.colonne[k]['value'] !== 'undefined') {
                if (typeof dataTable.data[i][dataTable.colonne[k]['id']] !== 'undefined') {
                    if (!headSet) {
                        _rowHead.append($('<th/>').html(dataTable.colonne[k]['value']).clone(true))
                    }
                    _row.append(new col().html(dataTable.data[i][dataTable.colonne[k]['id']]).clone(true))
                }
                k++
            }
            var l = 0
            if (!headSet && btnAction)
                _rowHead.append($('<th/>').html('Action').clone(true))
            headSet = true
            while (btnAction && btnAction[l]) {
                _row.append(new col().append(new bouttonAction(
                        {
                            class: btnAction[l].class,
                            title: btnAction[l].title,
                            action: btnAction[l].action,
                            session: {fnct: btnAction[l].callback, data: dataTable.data[i]},
                            callback: function (this_btn, action, callback) {
                                if (typeof callback.fnct === 'function')
                                    callback.fnct.apply(this, [this_btn, _row, callback.data, action])
                            }
                        }
                ).clone(true)).clone(true))
                l++
            }
            //notPaginate = (dataTable.maxRow > i)
            /*for (var _ in dataTable.data[i]) {
             _row.append(new col().html(_).clone(true))
             }*/
            _row.appendTo(content)
            i++
        }
        if (i == 0)
            return self
        head.append(_rowHead.clone(true))
        if (paginer)
            foot.append(
                    $('<tr/>').append(
                    $('<td/>',
                            {
                                'colspan': btnAction ? dataTable.colonne.length + 1 : dataTable.colonne.length
                            }
                    ).append(//dataTable.colonne
                    $('<ul/>',
                            {
                                'style': 'display: flex; padding-left: 0px;list-style: none; border-radius: .25rem;margin: 0px; float: right;'
                            }
                    ).append(
                    $('<li/>',
                            {
                                'style': 'display:inline;'
                            }
                    ).append(
                    $('<a/>',
                            {
                                'class': dataTable.pagination.link_prev ? 'clicable' : 'disabled',
                                'href': '#',
                                'style': 'position: relative;float: left;padding: 6px 12px;margin-left: -1px;line-height: 1.42857143;color: #337ab7;text-decoration: none;background: #fafafa;border: 1px solid #b0a7a7;margin-left: 0;border-top-left-radius: 4px;border-bottom-left-radius: 4px;'
                            }
                    ).html(bib_lang['paginate_prec']).clone(true)
                    ).click(
                    function () {
                        if (typeof event.pagPrec === 'function' && dataTable.pagination.link_prev) {
                            event.pagPrec.call(this, table, $(this), dataTable.pagination.link_prev)
                        }
                    }
            ).clone(true)
                    ).append(
                    $('<li/>',
                            {
                                'class': 'disabled', 'style': 'display:inline;'
                            }
                    ).append(
                    $('<a/>',
                            {
                                'class': dataTable.pagination.link_next ? 'clicable' : 'disabled',
                                'href': '#',
                                'style': 'position: relative;float: left;padding: 6px 12px;margin-left: -1px;line-height: 1.42857143;color: #337ab7;text-decoration: none;background: #fafafa;border: 1px solid #b0a7a7;border-top-right-radius: 4px;border-bottom-right-radius: 4px;'
                            }
                    ).html(bib_lang['paginate_suiv']).click(
                    function () {
                        if (typeof event.pagSuiv === 'function' && dataTable.pagination.link_next) {
                            event.pagSuiv.call(this, table, $(this), dataTable.pagination.link_next)
                        }
                    }
            ).clone(true)
                    ).clone(true)
                    ).clone(true)
                    ).clone(true)
                    ).clone(true)
                    )

        self.css('display', 'inline-block').html('').append(table.append(head.clone(true)).append(content.clone(true)).append(foot.clone(true)).clone(true))
        return self
    }

}
)
        ;
/*
 * 
 * 
 * $.extend( jQuery.expr[':'], {
 largeur : function(a, i, m){
 return $(a).css('width') == m[3]; // retourne les éléments
 ayant la largeur donnée
 }
 } );
 // exemple :
 $('elements:largeur("100px")'); // sélectionne seulement les
 éléments ayant une largeur de 100px exactement
 */
var biblio = new Bibliotheque()
biblio.add(
        {NUMERO_CIN: 'Numéro CIN'},
        {CODE_DIRECTION: 'Référence direction'},
        {CIN_LIEU_DELIVRE: 'CIN délivrer à'},
        {NOM: 'Nom'},
        {PHOTO: 'Photo'},
        {LIEU_NAISSANCE: 'Lieu de naissance'},
        {ADRESSE_MAIL: 'E-mail'},
        {NATIONALITE: 'Natinalité'},
        {SITUATION_MATRIMONIALE: 'Situation matrimoniale'},
        {created_at: 'Créer le'},
        {CODE_CATEGORIE_PROFESSIONNELLE: 'Référence catégorie'},
        {CODE_POSTAL: 'Code postal'},
        {CIN_DATE_DELIVRE: 'Date CIN'},
        {PRENOM: 'Prénom(s)'},
        {ADRESSE: 'Adresse'},
        {FONCTION: 'Fonction'},
        {SEXE: 'Séxe'},
        {NOMBRE_ENFANT: 'Nombre d\'enfant'},
        {updated_at: 'A jour le'},
        {FONCTIONs: 'Fonction'},
        {FONCTIONv: 'Fonction'},
        {NOM_VILLE: 'Ville'},
        {DATE_NAISSANCE: 'Date de naisssance'},
        {NUMERO_TELEPHONE: 'Contact mobile'},
        {NOM_DIRECTION: 'Direction'}
)



