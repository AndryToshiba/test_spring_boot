/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function ($) {
    $.fn.PanelTable = function (options) {

        var parametres = $.extend(true, {}, {
            dataId: '000000',
            iconImg: 'img/avatar.png',
            headerContent: 'manitra header',
            bodyContent: $('<div/>').html('Panel table'),
            newElement: 0,
            btnCallback: null
        }, options = options || {}), element = $(this);

//
        var panel = function (parms) {
            var _id = 'panel-' + $(this).generateChars(10)
            return {
                'Panel': $('<div/>', {
                    'class': 'panel panel-default no-padding panel-table-list',
                    'panel-table': _id
                }),
                'PanelHeader': $('<div/>', {
                    'class': 'panel-heading panel-heading-table-list'
                }).append(
                        $('<h4/>', {
                            'class': 'panel-title',
                            'style': 'display:inline-block;'
                        }).append(
                        $('<i class="fa fa-photo-icon"><img src="' + (typeof parms.iconImg === 'string' ? parms.iconImg : 'img/avatar.png') + '"/></i>').clone('true')
                        ).append(
                        $('<a/>', {
                            'data-toggle': 'collapse',
                            'data-parent': '#accordion',
                            'href': "#" + _id
                        }).html(parms.headerContent).click(function () {
                    $('[data-toggle="collapse"]').trigger('rendreWidth')
                }).clone(true)

                        ).clone(true)

                        ).append(
                        $('<div/>', {
                            'class': 'pull-right box-tools btn-remove',
                            'style': 'margin-top:0px;margin-bottom:0px;'
                        }).append(
                        $('<button/>', {
                            'type': 'button', 'class': 'btn btn-info btn-sm',
                            'title': 'modifier',
                            'style': 'font-size: 8px;'
                        }).addTitle().click(function () {
                    if (typeof parms.btnCallback === 'function')
                        parms.btnCallback.call(this, $(this), $('div[panel-table="' + _id + '"]'), parms.dataId)
                }).append(
                        $('<i/>', {'class': 'fa fa-edit'}).clone(true)
                        ).clone(true)
                        ).clone(true)
                        ),
                'PanelBody': $('<div/>', {
                    'class': 'panel-collapse collapse panel-table-list-content',
                    'id': _id
                }).append(
                        $('<div/>', {
                            'class': 'panel-body'
                        }).append(parms.bodyContent.clone(true))
                        )
            }
        }
//

        element.each(function () {
            var _i_ = new panel(parametres);
            _i_.PanelHeader.appendTo(_i_.Panel);
            _i_.PanelBody.appendTo(_i_.Panel);
            if (parametres.newElement === 0)
                $(this).html('');
            _i_.Panel.appendTo($(this));
        })

    }
}
)

