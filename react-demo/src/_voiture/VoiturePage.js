import React, { Component, createRef } from "react";
import { TemplateVoiture } from "./TemplateVoiture";
import HttpServeur from "../_serviceHttp/HttpServeur";
import LocalStorageService from "../_serviceHttp/LocalStorageService";
import "../_helpers/EventButtonCliked";
import $ from 'jquery';
import form from "../_helpers/FormHelper";
import ComponentExtend from "../ComponentExtend";

const localStorageService = LocalStorageService.getService();

class VoiturePage extends ComponentExtend {

    constructor(props) {
        super(props);
        this.api = new HttpServeur({
            postFixUrl: 'commentaire/add'
        });
        this.apiListGuest = new HttpServeur({
            postFixUrl: 'voiture/liste-guest'
        });
        this.apiListAuth = new HttpServeur({
            postFixUrl: 'voiture/liste-auth'
        });
        this.form = createRef();
        this.submit = this.submit.bind(this);
        this.state = { voitures: [], isSend: false }
    }

    componentDidMounts() {
        if (localStorageService.getAuthUser()) {
            this.setState({ isSend: true })
            this.list(this.apiListAuth);
        } else {
            this.list(this.apiListGuest);
        }
    }

    list = (api) => {
        this.setState({ voitures: [] });
        api.post({}).then(res => {
            const data = [...res.data];
            let slice = [], datas = [];
            data.forEach((val, index) => {
                slice.push(val)
                if ((index + 1) % 3 == 0) {
                    this.setState({ voitures: [...this.state.voitures, slice] });
                    slice = [];
                }
            });
            this.setState({ voitures: [...this.state.voitures, slice] });
        }).catch(error => {
            //console.log(error)
        }).finally(() => {
            //loading
        });
    }

    submit = (event) => {
        let loading = $(event.target).InputLoadRequest();
        event.preventDefault();
        const curentForm = $(event.target).parents('form');
        if (!curentForm.length) return;
        const data = form(curentForm[0]).serialized();
        form(curentForm[0]).removeErreur();
        this.api.post(data).then(res => {
            if (localStorageService.getAuthUser()) {
                this.setState({ isSend: true })
                this.list(this.apiListAuth);
            } else {
                this.list(this.apiListGuest);
            }
        }).catch(error => {
            form(curentForm[0]).erreur(error);
        }).finally(() => {
            loading.remove();
        });
    }

    render() {
        return (
            <TemplateVoiture state={this.state} submit={{ submit: this.submit }} formRef={{ form: this.form }} />
        )
    }
}

export default VoiturePage;