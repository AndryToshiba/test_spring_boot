import React, { createRef, useRef } from 'react';

export const TemplateVoiture = ({ state, submit, formRef = null }) => {
    return (
        <div>
            <section className="background-cover bg-dark pb-5 position-relative pt-5 text-white" style={{ backgroundImage: "url('img/ct_sea-418742_1920.jpg')" }}>
                <div className="container mt-5 pt-4">
                    <div className="row">
                        <div className="col-md">
                            <h1 className="text-uppercase">Voitures publiées</h1>
                        </div>
                    </div>
                </div>
            </section>
            <section className="py-5">
                <div className="container">
                    {state.voitures.map(values => (
                        <div className="justify-content-center row">
                            {
                                values.map(value => (
                                    <div className="col-md-4 py-3">
                                        <div className="align-items-center background-center-center background-cover d-flex h-80 justify-content-center px-2 py-5" style={{ backgroundImage: `url('${value.image}')`, backgroundSize: '100% 100%' }}>
                                            <div>
                                                <a href="#" className="text-center text-decoration-none text-white"><div>
                                                    <div>
                                                        <h3 className="text-shadow">{value.model}</h3>
                                                    </div>
                                                </div></a>
                                            </div>
                                        </div>
                                        <div className="h-10" style={{ maxHeight: '150px', overflowY: 'auto', overflowX: 'auto', margin: '5px 0' }} >
                                            {value.commentaire.map(val => {
                                                if (val.message) {
                                                    return (
                                                        <div style={{ display: 'flex', alignItems: 'center', margin: '5px 0', overflowX: 'auto', whiteSpace: 'nowrap' }}>
                                                            <span className="fa fa-user" style={{ border: '1px solid black', width: '35px', height: '35px', borderRadius: '50%', fontSize: '30px', textAlign: 'center' }}></span>
                                                            <p style={{ margin: '0 0 0 5px' }}>{val.message}</p>
                                                        </div>
                                                    )
                                                }
                                            })
                                            }
                                        </div>
                                        {state.isSend &&
                                            <form className="h-10" style={{ display: 'flex', alignItems: 'center', margin: '5px 0' }}>
                                                <input type="text" name="voitureId" style={{ display: "none" }} value={value.id} />
                                                <input type="text" name="message" style={{ width: '90%' }} /><button type="submit" onClick={($event) => { submit.submit($event) }} style={{ width: '10%' }}><i className="fa fa-send"></i></button>
                                            </form>
                                        }
                                    </div>
                                ))
                            }
                        </div>
                    ))
                    }
                </div>
            </section>
        </div>
    )
}