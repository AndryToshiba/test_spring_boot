import React, { Component, createRef } from "react";
import { TemplatePassword } from "./TemplatePassword";
import HttpServeur from "../_serviceHttp/HttpServeur";
import LocalStorageService from "../_serviceHttp/LocalStorageService";
import "../_helpers/EventButtonCliked";
import $ from 'jquery';
import form from "../_helpers/FormHelper";
import ComponentExtend from "../ComponentExtend";
import { connect } from "react-redux";

const localStorageService = LocalStorageService.getService();
const mapsStateToProps = (state) => {
    return state
}, mapDispatchToProps = (dispatch) => {
    return { dispatch: (action) => { dispatch(action) } }
}

class PasswordPage extends ComponentExtend {

    constructor(props) {
        super(props);
        localStorageService.clearAuthUser();
        this.api = new HttpServeur({
            postFixUrl: 'users/password'
        });
        this.apiConfirm = new HttpServeur({
            postFixUrl: 'users/confirmPassword'
        });
        this.form = createRef();
        this.formConfirm = createRef();
        this.submit = this.submit.bind(this);
        this.submitConfirm = this.submitConfirm.bind(this);
        this.state = {form:true,formConfirm:false,email:""}
    }

    submit = (event) => {
        let loading = $(event.target).InputLoadRequest();
        event.preventDefault();
        const data = form(this.form.current).serialized();
        form(this.form.current).removeErreur();
        this.api.post(data).then(res => {
            this.setState({form:false,formConfirm:true,email:res.data.email})
        }).catch(error => {
            form(this.form.current).erreur(error); 
        }).finally(() => {
            loading.remove();
        });
    }

    submitConfirm = (event) => {
        let loading = $(event.target).InputLoadRequest();
        event.preventDefault();
        const data = form(this.formConfirm.current).serialized();
        if(data.password != data.confirmPassword) {
            form(this.formConfirm.current).erreur({
                confirmPassword:"Password invalid"
            }); 
            loading.remove();
            return;
        }
        form(this.formConfirm.current).removeErreur();
        this.apiConfirm.post(data).then(res => {
            this.props.updateState({isLoggedIn:true})
            localStorageService.setAuthUser(res.data);
            const { from } = this.props.location.state || { from: { pathname: "/voiture" } };
            this.props.history.push(from);
        }).catch(error => {
            form(this.formConfirm.current).erreur(error); 
        }).finally(() => {
            loading.remove();
        });
    }

    render() {
        return (
                <TemplatePassword state={this.state} submit={{ submit: this.submit, submitConfirm:this.submitConfirm }} formRef={{ form: this.form, formConfirm: this.formConfirm }} />
        )
    }
}

export default connect(mapsStateToProps, mapDispatchToProps)(PasswordPage);