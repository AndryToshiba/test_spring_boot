import React, { createRef, useRef } from 'react';
import { Link } from 'react-router-dom';

export const TemplatePassword = ({ state, submit, formRef = null }) => {
    return (
        <div>
            <section className="background-cover bg-dark pb-5 position-relative pt-5 text-white" style={{ backgroundImage: "url('img/ct_sea-418742_1920.jpg')" }}>
                <div className="container mt-5 pt-4">
                    <div className="row">
                        <div className="col-md">
                            <h1 className="text-uppercase">MOT DE PASSE OUBLIER</h1>
                        </div>
                    </div>
                </div>
            </section>
            <section className="py-5">
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-md-4">

                            <p className="text-center">Mot de passe oublier</p>

                            {state.form &&
                                <form ref={formRef.form}>
                                    <div className="input-group mb-3">
                                        <input type="email" className="form-control" placeholder="Email" id="email" name="email" autofocus="" required="" />
                                        <div className="input-group-append">
                                            <div className="input-group-text">
                                                <span className="fa fa-envelope"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="row">
                                        <div className="col-4"></div>

                                        <div className="col-8 text-right">
                                            <input className="btn btn-primary" type="submit" value="Verifier email" onClick={submit.submit} />
                                        </div>

                                    </div>

                                    <div>
                                        <p className="mb-1">
                                            <Link to="/login" className="mr-2">déja un compte ?</Link>
                                        </p>
                                    </div>

                                </form>
                            }

                            {state.formConfirm &&
                                <form ref={formRef.formConfirm}>
                                    <div className="input-group mb-3">
                                        <input type="email" name="email" value={state.email} style={{display:"none"}} />
                                        <input type="password" className="form-control" placeholder="Password" name="password" id="password" required="" />
                                        <div className="input-group-append">
                                            <div className="input-group-text">
                                                <span className="fa fa-lock"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="input-group mb-3">
                                        <input type="password" className="form-control" placeholder="Confirm password" name="confirmPassword" id="confirmPassword" required="" />
                                        <div className="input-group-append">
                                            <div className="input-group-text">
                                                <span className="fa fa-lock"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-4"></div>
                                        <div className="col-8 text-right">
                                            <input className="btn btn-primary" type="submit" value="Changer mot de passe" onClick={submit.submitConfirm} />
                                        </div>
                                    </div>
                                </form>
                            }
                        </div>
                    </div>
                </div>
            </section>
        </div >
    )
}