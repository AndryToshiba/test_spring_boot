import React from "react";
import { Link } from "react-router-dom";

export const NavigationTop = ({ state, fn }) => (
    <nav className="fixed-top navbar navbar-dark navbar-expand-lg navbar-top">
        <div className="container-fluid text-uppercase">
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown-3" aria-controls="navbarNavDropdown-3" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse " id="navbarNavDropdown-3">
                <ul className="navbar-nav ">
                    <li className="mx-lg-1 nav-item">
                        <Link to="/voiture" className="nav-link text-shadow text-white">Voiture</Link>
                    </li>
                    {state.isLoggedIn &&
                        <li className="mx-lg-1 nav-item">
                            <Link to="/publier-voiture" className="nav-link text-shadow text-white">Publier voiture</Link>
                        </li>
                    }
                </ul>
                <ul className="align-items-lg-center ml-auto navbar-nav">
                    {!state.isLoggedIn &&
                        <li className="nav-item">
                            <Link to="login" className="nav-link text-shadow text-white"><i className="fa-lg fa-user-circle fa mr-2"></i>Se connecter</Link>
                        </li>
                    }
                    {!state.isLoggedIn &&
                        <li className="nav-item">
                            <Link to="register" className="nav-link text-shadow text-white" ><i className="fa-lg fa-user-circle fa mr-2"></i>S'inscrire</Link>
                        </li>

                    }
                    {state.isLoggedIn &&
                        <li className="nav-item">
                            <Link className="nav-link text-shadow text-white" to="login"><i className="fa-lg fa-sign-out-alt fa mr-2"></i>Se déconnecter</Link>
                        </li>}
                </ul>
            </div>
        </div>
    </nav>

);