import React, { Component, createRef } from "react";
import { TemplateRegister } from "./TemplateRegister";
import HttpServeur from "../_serviceHttp/HttpServeur";
import LocalStorageService from "../_serviceHttp/LocalStorageService";
import "../_helpers/EventButtonCliked";
import $ from 'jquery';
import form from "../_helpers/FormHelper";
import ComponentExtend from "../ComponentExtend";

const localStorageService = LocalStorageService.getService();

class RegisterPage extends ComponentExtend {

    constructor(props) {
        super(props);
        localStorageService.clearAuthUser();
        this.api = new HttpServeur({
            postFixUrl: 'users/register'
        });
        this.form = createRef();
        this.submit = this.submit.bind(this);
    }

    submit = (event) => {
        let loading = $(event.target).InputLoadRequest();
        event.preventDefault();
        const data = form(this.form.current).serialized();
        if(data.password != data.confirmPassword) {
            form(this.form.current).erreur({
                confirmPassword:"Password invalid"
            }); 
            loading.remove();
            return;
        }
        form(this.form.current).removeErreur();
        this.api.post(data).then(res => {
            localStorageService.setAuthUser(res);
            const { from } = this.props.location.state || { from: { pathname: "login" } };
            this.props.history.push(from);
        }).catch(error => {
            form(this.form.current).erreur(error); 
        }).finally(() => {
            loading.remove();
        });
    }

    render() {
        return (
                <TemplateRegister submit={{ submit: this.submit }} formRef={{ form: this.form }} />
        )
    }
}

export default RegisterPage;