import LocalStorageService from "../_serviceHttp/LocalStorageService";

const initialState = { authUser: null, coordonnees: [], new_coordonnees: [], directionMaps: null, updateResult: null };

function StateStore(state = initialState, action) {
    let nextState
    switch (action.type) {
        case 'IS_AUTH': 
            const user_ = action.value;
            if (!user_) {
                nextState = {
                    ...state,
                    authUser: null
                }
            } else {
                nextState = {
                    ...state,
                    authUser: user_
                }
            }
            return nextState || state
        case 'DIRECTION_MAPS':
            let wayPoints = [], origin = null, destination = null
            for (let x = 0; x < action.value.length && action.value.length > 2; x++) {
                if (x === 0)
                    origin = new window.google.maps.LatLng(action.value[x].LATITUDE, action.value[x].LATITUDE)
                else if (x === action.value.length - 1)
                    destination = new window.google.maps.LatLng(action.value[x].LATITUDE, action.value[x].LATITUDE)
                else
                    wayPoints.push({
                        location: new window.google.maps.LatLng(action.value[x].LATITUDE, action.value[x].LATITUDE),
                        stopover: true
                    })
            }
            nextState = {
                ...state,
                directionMaps: (origin && destination) ? { origin: origin, destination: destination, waypoints: wayPoints } : null,
                updateResult: action.updateResult ? action.updateResult : null
            }
            return nextState || state
        case 'ADD_COORDONNEE':
            const coordonneesIndex = state.coordonnees.filter(item => item.ID_COORDONNEES === action.value.ID_COORDONNEES)
            if (coordonneesIndex === -1) {
                nextState = {
                    ...state,
                    coordonnees: [...state.coordonnees, { ...action.value, showInfo: false }]
                }
            } else {
                nextState = {
                    ...state,
                    coordonnees: state.coordonnees
                }
            }
            return nextState || state
        case 'ADD_COORDONNEE_MAP':
            let new_index = state.new_coordonnees.length ? parseInt(state.new_coordonnees[parseInt(state.new_coordonnees.length) - 1].ID_COORDONNEES) : 0
            nextState = {
                ...state,
                new_coordonnees: [...state.new_coordonnees, { ...action.value, ID_COORDONNEES: new_index + 1, showInfo: false }]
            }
            return nextState || state
        case 'COORDONNEE_MAP':
            nextState = {
                ...state,
                new_coordonnees: action.value.map((data) => {
                    return { ...data, showInfo: false }
                })
            }
            return nextState || state
        case 'UPDATE_COORDONNEE_MAP':
            nextState = {
                ...state,
                new_coordonnees: state.new_coordonnees.map((data, index) => {
                    if (index === action.index)
                        data = action.value
                    return data
                })
            }
            return nextState || state

        case 'REMOVE_COORDONNEE_MAP':
            nextState = {
                ...state,
                new_coordonnees: state.new_coordonnees.filter((data, index) => {
                    return index !== action.index
                })
            }
            return nextState || state

        case 'SHOW_COORDONNEE_INFO':

            nextState = {
                ...state,
                coordonnees: state.coordonnees.map((data, index) => {
                    if (index === action.index)
                        data.showInfo = !data.showInfo
                    return data
                })
            }
            return nextState || state
        case 'SHOW_COORDONNEE_INFO_MAP':

            nextState = {
                ...state,
                new_coordonnees: state.new_coordonnees.map((data, index) => {
                    if (index === action.index)
                        data.showInfo = !data.showInfo
                    return data
                })
            }
            return nextState || state
        case 'COORDONNEES':
            nextState = {
                ...state,
                coordonnees: action.value.map((data) => {
                    return { ...data, showInfo: false }
                })
            }
            return nextState || state
        default:
            return state
    }
}

export default StateStore;