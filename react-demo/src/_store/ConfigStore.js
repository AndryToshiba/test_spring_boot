import { createStore } from 'redux';
import StateStore from './StateStore';

export default createStore(StateStore)