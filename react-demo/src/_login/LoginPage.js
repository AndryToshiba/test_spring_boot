import React, { Component, createRef } from "react";
import { TemplateLogin } from "./TemplateLogin";
import HttpServeur from "../_serviceHttp/HttpServeur";
import LocalStorageService from "../_serviceHttp/LocalStorageService";
import "../_helpers/EventButtonCliked";
import $ from 'jquery';
import form from "../_helpers/FormHelper";
import ComponentExtend from "../ComponentExtend";
import { connect } from "react-redux";

const localStorageService = LocalStorageService.getService();
const mapsStateToProps = (state) => {
    return state
}, mapDispatchToProps = (dispatch) => {
    return { dispatch: (action) => { dispatch(action) } }
}

class LoginPage extends ComponentExtend {

    constructor(props) {
        super(props);
        localStorageService.clearAuthUser();
        this.api = new HttpServeur({
            postFixUrl: 'users/login'
        });
        this.form = createRef();
        this.submit = this.submit.bind(this);
    }

    submit = (event) => {
        let loading = $(event.target).InputLoadRequest();
        event.preventDefault();
        console.log(form(this.form.current));
        const data = form(this.form.current).serialized();
        form(this.form.current).removeErreur();
        this.api.post(data).then(res => {
            this.props.updateState({isLoggedIn:true})
            localStorageService.setAuthUser(res.data);
            const { from } = this.props.location.state || { from: { pathname: "/voiture" } };
            this.props.history.push(from);
        }).catch(error => {
            form(this.form.current).erreur(error); 
        }).finally(() => {
            loading.remove();
        });
    }

    render() {
        return (
                <TemplateLogin submit={{ submit: this.submit }} formRef={{ form: this.form }} />
        )
    }
}

export default connect(mapsStateToProps, mapDispatchToProps)(LoginPage);