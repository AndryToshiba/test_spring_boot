import React, { createRef, useRef } from 'react';
import { Link } from 'react-router-dom';

export const TemplateLogin = ({ submit, formRef = null }) => {
    return (
        <div>
            <section className="background-cover bg-dark pb-5 position-relative pt-5 text-white" style={{ backgroundImage: "url('img/ct_sea-418742_1920.jpg')" }}>
                <div className="container mt-5 pt-4">
                    <div className="row">
                        <div className="col-md">
                            <h1 className="text-uppercase">CONNEXION</h1>
                        </div>
                    </div>
                </div>
            </section>
            <section className="py-5">
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-md-4">

                            <p className="text-center">Se connecter</p>

                            <form ref={formRef.form}>
                                <div className="input-group mb-3">
                                    <input type="email" className="form-control" placeholder="Email" id="email" name="email" autofocus="" required="" />
                                    <div className="input-group-append">
                                        <div className="input-group-text">
                                            <span className="fa fa-envelope"></span>
                                        </div>
                                    </div>
                                </div>
                                <div className="input-group mb-3">
                                    <input type="password" className="form-control" placeholder="Password" name="password" id="password" required="" />
                                    <div className="input-group-append">
                                        <div className="input-group-text">
                                            <span className="fa fa-lock"></span>
                                        </div>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-8"></div>

                                    <div className="col-4 text-right">
                                        <input className="btn btn-primary" type="submit" value="Login" onClick={submit.submit} />
                                    </div>

                                </div>

                                <div>
                                    <p className="mb-1">
                                        <Link to="/password" className="mr-2">Mot de passe oublier?</Link>
                                    </p>
                                    <p className="mb-0">
                                        <Link to="/register" className="text-center">S'inscrire?</Link>
                                    </p>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </section>
        </div >
    )
}