import React from 'react';
import { Route, Redirect, Switch } from 'react-router-dom';
import LocalStorageService from './_serviceHttp/LocalStorageService';
import LoginPage from './_login/LoginPage';
import VoiturePage from './_voiture/VoiturePage';
import NotFound from './PageErreur/NotFound';
import RegisterPage from './_register/RegisterPage';
import AjoutVoiturePage from './_voitureAjout/AjoutVoiturePage';
import PasswordPage from './_password/PasswordPage';

const auth = LocalStorageService.getService();

export const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={props => {
        if (rest.state && rest.updateState) rest.updateState(rest.state)

        return (
            auth.getAuthUser()
                ? <Component {...props} {...rest.pushProps} />
                : <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
        )
    }} />
)

export const PrivateRouteAdmin = ({ component: Component, ...rest }) => (
    <Route {...rest} render={props => {
        if (rest.state && rest.updateState) rest.updateState(rest.state)

        return (
            auth.getAuthUser() && auth.getAuthUser().role == "Admin"
                ? <Component {...props} {...rest.pushProps} {...rest} />
                : <Redirect to={{ pathname: '/login', state: { ...rest , from: props.location} }} />
        )
    }} />
)

export const PublicRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={props => {
        if (rest.state && rest.updateState) rest.updateState(rest.state)
        return (<Component {...props} {...rest} />)
    }} />
)

export const Routes = (updateState) => (
    <Switch>
        <PublicRoute exact path="/" component={LoginPage}  pushProps {...updateState} /> /* path="/:filter?" */
        <PublicRoute exact path="/login" component={LoginPage}  pushProps {...updateState} /> /* path="/:filter?" */
        <PublicRoute exact path="/register" component={RegisterPage} /> /* path="/:filter?" */
        <PublicRoute exact path="/password" component={PasswordPage}  pushProps {...updateState}  /> /* path="/:filter?" */
        <PrivateRoute exact path="/publier-voiture" component={AjoutVoiturePage} pushProps {...updateState} state={{ }} />
        <PublicRoute exact path="/voiture" component={VoiturePage} pushProps {...updateState} state={{}} />
        <PublicRoute component={NotFound} {...updateState} state={{navTop:false,footer:false}} />
    </Switch>
);