const LocalStorageService = (function () {
    var _service;
    function _getService() {
        if (!_service) {
            _service = this;
            return _service;
        }
        return _service;
    }

    function _setAuthUser(user) {
        localStorage.setItem('user_auth', JSON.stringify(user));
    }

    function _getAuthUser() {
        var user = localStorage.getItem('user_auth');
        return user === 'undefined' ? null: JSON.parse(user);
    }

    function _getAccessToken() {
        var user = _getAuthUser() ;
        return user ? user.api_token : null;
    }

    function _clearAuthUser() {
        localStorage.removeItem('user_auth');
    }

    return {
        getService: _getService,
        setAuthUser: _setAuthUser,
        getAuthUser: _getAuthUser,
        clearAuthUser: _clearAuthUser,
        getAccessToken: _getAccessToken
    }
})();

export default LocalStorageService;