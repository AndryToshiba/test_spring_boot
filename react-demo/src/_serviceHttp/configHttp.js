import React from 'react';
import axios from "axios";
import { Component } from "react";
import LocalStorageService from "./LocalStorageService";

export const URL_BASE = "http://127.0.0.1:8080/api/";
export const URL_IMAGE = function (path, name, type) {
    return "http://127.0.0.1:8080/" + path + "/" + name + "." + type;
};
export const controleStatusHttp = function (code, text) {
    if (code === 401 && text === 'Unauthorized')
        //router.history.push({ from: { pathname: "/aceuil" } }); <Redirect to={{pathname:'/', state:{id:'123'}}}/> ==== this.props.location.state.id
        window.location = '/login';
}
const localStorageService = LocalStorageService.getService();

const configServeur = (function () {
    axios.interceptors.request.use(
        config => {
            const token = localStorageService.getAccessToken();
            if (token) {
                config.headers['Authorization'] = 'Bearer ' + token;
            }
            config.headers['Content-Type'] = 'application/json';
            //config.headers['Accept'] = 'application/json';
            config.baseURL = URL_BASE;
            return config;
        },
        error => {
            Promise.reject(error)
        }
    );

    axios.interceptors.response.use(
        (response) => {
            return response.data
        },
        function (error) {
            const originalRequest = error.config;
            if (error.response.status === 401) {
                window.location = '/login';
                return Promise.reject(error);
            }
            if (error.response.status === 422) {
                return Promise.reject({ error: 422, data: error.response.data.errors })
            }
            return Promise.reject(error);
        }
    );
    return axios;

})()

export default configServeur;

/*

axios.interceptors.request.use(
    config => {
        const token = localStorageService.getAccessToken();
        if(token){
            config.headers['Authorization'] = 'Bearer '+ token;
        }
        config.headers['Content-Type'] = 'application/json';
        return config;
    },
    error => {
        Promise.reject(error)
    }
)

axios.interceptors.response.use(
    (response)=>{
    return response
    },
    function(error){
        const originalRequest = error.config;
        if(error.response.status === 401 && originalRequest.url === 'http://localhost/auth//token'){
            router.push('/login'); //import router
            return Promise.reject(error);
        }
        if(error.response.status === 401 && !originalRequest._retry){
            originalRequest._retry = true;
            const refreshToken = localStorageService.getRefreshToken();
            return axios.post('/auth/token',{
                "refresh_token":refreshToken
            }).then(res=>{
                if(res.status === 201){
                    localStorageService.setToken(res.data);
                    axios.defaults.headers.common['Authorieation'] = 'Bearer '+localStorageService.getAccessToken();
                    return (originalRequest);
                }
            })
        }
        return Promise.reject(error);
    }
)

*/

/*redirect router
    axios.interceptors.response.use(
        function(response){
            return response;
        },function(error){
            if(error.response.status === 403){
                console.log('Redirect route');
                return Promise.reject(error);
            }
        }
        )

*/