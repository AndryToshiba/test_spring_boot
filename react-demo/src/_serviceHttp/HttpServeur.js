import React, { Component } from "react";
import configServeur from "./configHttp";

const api = configServeur;

class HttpServeur extends Component {

    constructor(config) {
        super(config);
        this.postFixUrl = config.postFixUrl
    }

    get(id) {
        api.get(`${this.postFixUrl}/${id}`)
            .then(res => {
                const persons = res.data;
                this.setState({ persons });
            }).catch(e => {
                console.log(e);
            })
    }

    postPaginate(page, data) {
        return api.post(`${this.postFixUrl}?page=${page}`, data);
    }

    post(data) {
        return api.post(`${this.postFixUrl}`, data);
    }

    delete() {
        api.delete(`https://jsonplaceholder.typicode.com/users/${this.state.id}`)
            .then(res => {
                console.log(res);
                console.log(res.data);
            })
    }

}

export default HttpServeur;