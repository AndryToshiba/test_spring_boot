import React, { Component, createRef } from "react";
import { TemplateAjoutVoiture } from "./TemplateAjoutVoiture";
import HttpServeur from "../_serviceHttp/HttpServeur";
import LocalStorageService from "../_serviceHttp/LocalStorageService";
import "../_helpers/EventButtonCliked";
import $ from 'jquery';
import form from "../_helpers/FormHelper";
import ComponentExtend from "../ComponentExtend";

const localStorageService = LocalStorageService.getService();

class AjoutVoiturePage extends ComponentExtend {

    constructor(props) {
        super(props);
        this.api = new HttpServeur({
            postFixUrl: 'voiture/publier'
        });
        this.form = createRef();
        this.submit = this.submit.bind(this);
        this.upload = this.upload.bind(this)
        this.state = { imageSrc: "" }
    }

    componentDidMounts() {
        console.log(localStorageService.getAuthUser())
    }

    submit = (event) => {
        let loading = $(event.target).InputLoadRequest();
        event.preventDefault();
        const data = form(this.form.current).serialized();
        form(this.form.current).removeErreur();
        data.image = this.state.imageSrc;
        this.api.post(data).then(res => {
            this.props.history.push({ pathname: "/voiture" });
        }).catch(error => {
            form(this.form.current).erreur(error);
        }).finally(() => {
            loading.remove();
        });
    }

    upload = (event) => {
        let reader = new FileReader();
        reader.readAsDataURL(event.target.files[0]);
        reader.onload = (data) => {
            this.setState({ imageSrc: data.target.result })
        }

    }

    render() {
        return (
            <TemplateAjoutVoiture state={this.state} submit={{ submit: this.submit, upload: this.upload }} formRef={{ form: this.form }} />
        )
    }
}

export default AjoutVoiturePage;