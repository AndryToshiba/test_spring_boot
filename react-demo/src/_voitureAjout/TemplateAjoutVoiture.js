import React, { createRef, useRef } from 'react';
import './AjoutVoiturePage.css'

export const TemplateAjoutVoiture = ({ state, submit, formRef = null }) => {
    return (
        <div>
            <section className="background-cover bg-dark pb-5 position-relative pt-5 text-white" style={{ backgroundImage: "url('img/ct_sea-418742_1920.jpg')" }}>
                <div className="container mt-5 pt-4">
                    <div className="row">
                        <div className="col-md">
                            <h1 className="text-uppercase">Excursions</h1>
                        </div>
                    </div>
                </div>
            </section>
            <section className="py-5">
                <div className="container">
                    <div className="justify-content-center row">
                        <form  ref={formRef.form} style={{display:'contents'}}>
                            <div className="col-md py-3">
                                <div className="align-items-center background-center-center background-cover d-flex h-100 justify-content-center px-2 py-5" style={{ backgroundImage: `url('${state.imageSrc}')`, backgroundColor: 'gray', backgroundSize: '100% 100%' }}>
                                    <div style={{ color: 'white', textAlign: 'center' }}>
                                        <div>
                                            <div style={{ position: 'relative', height: 40 + 'px', width: 0, margin: 'auto' }}>
                                                <input type="file" id="image" name="image" required="" style={{ position: 'absolute', width: 45 + 'px', height: 45 + 'px', zIndex: 3, cursor: 'pointer', opacity: 0 }} onChange={submit.upload} />
                                                <span className="fa fa-edit" style={{ fontSize: 45 + 'px', position: 'absolute' }}></span>
                                            </div>
                                            <div>
                                                <h4 className="text-shadow">Selectionner image</h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-8 py-3">
                                <div className="input-group mb-3">
                                    <input type="text" className="form-control" placeholder="Model" id="firstName" name="model" autofocus="" required="" />
                                    <div className="input-group-append">
                                        <div className="input-group-text">
                                            <span className="fa fa-edit"></span>
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-8">

                                    </div>

                                    <div className="col-4 text-right">
                                        <input className="btn btn-primary" type="submit" value="Publier" onClick={submit.submit} />
                                    </div>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </section>
        </div>
    )
}