import React from "react";
const NotFound = () =>
    <div className="container container-height-fill bg-image login-img1"  style={{display:'flex',alignItems:'center',height:100+'vh'}}>
        <div className="row justify-content-center" style={{margin:'auto'}}>
            <section className="content">
                <div className="error-page">
                    <h2 className="headline text-yellow"> 404</h2>

                    <div className="error-content">
                        <h3><i className="fa fa-warning text-yellow"></i> Oops! Page not found.</h3>
                    </div>

                </div>

            </section>
        </div>
    </div>

export default NotFound;