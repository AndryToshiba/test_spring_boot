import React, { Component } from 'react';
import logo from './logo.svg';
import $ from 'jquery';
import './App.css';
import { Provider } from 'react-redux';
import ProtoTypes from 'prop-types';
import Store from './_store/ConfigStore';

import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { Routes } from './ProtectedRoute';
import { NavigationTop } from './NavigationTop';
import { Footer } from './Footer';
import LocalStorageService from './_serviceHttp/LocalStorageService';

const localStorageService = LocalStorageService.getService();

const Renders = ({ store, state, updateState }) => (
  <Provider store={store}>
    <Router>

      {state.navTop && <NavigationTop state={state}/>}
      
      <main id="app">
        <Routes updateState={updateState} />
      </main>
      {state.footer && <Footer/>}
    </Router>
  </Provider>
);

Renders.prototype = {
  store: ProtoTypes.object.isRequired
};

class App extends Component {

  /*componentDidMount() {
    const script = document.createElement('script');
    script.async = true;
    script.src = "js/vertical-align.js";
    document.head.appendChild(script);
  }*/

  constructor(props) {
    super(props);
    this.state = {
      isLoggedIn: false,
      user: {},
      navLeft: false,
      navTop: true,
      footer:true
    };
    this.updateState = this.updateState.bind(this)
  }

  updateState = (data) => {
    try {
      this.setState(data)
    } catch (error) {
    }
  }


  componentDidMount() {
    if(localStorageService.getAuthUser()) this.setState({isLoggedIn:true});
  }

  render() {
    return (
      <div id="apps">
        <Renders store={Store} state={this.state} updateState={this.updateState} />
      </div>

    );
  }
}
export default App;

/*
<Redirect to={{pathname:'/to-path',state:{from:props.location}}}    //this.props.location.state
*/