import React, { Component, Children } from "react";
import { updateMonJS } from "./_helpers/globale";
import { connect } from "react-redux";


class ComponentExtend extends Component {
    constructor(props) {
        super(props);
        this.state = {
            load: false
        }
        this.udateStasutJS = this.udateStasutJS.bind(this)
    }

    componentDidMount() {
        this.setState({ load: true })
        this.udateStasutJS()
        if (this.componentDidMounts) this.componentDidMounts();
    }
    shouldComponentUpdate(nextProps, nextState, nextContext) {
        return true
    }
    componentWillReceiveProps(nextProps, nextContext) {
        if (this.state.load) {
            this.udateStasutJS()
            this.setState({ load: false })

        }
        var change = false
        console.log(nextProps,nextContext)
        for (var i in nextProps) {
            if (nextProps[i] !== this.props[i] && ['history', 'location', 'match', 'staticContext'].indexOf(i) === -1) {
                change = true
            }
        }
        if (change && this.childreWillReceiveProps) this.childreWillReceiveProps(nextProps, nextContext);
    }

    udateStasutJS = () => {
        updateMonJS();
    }
}

export default ComponentExtend